function [cparams FBCerr FBCtime]= evalFBC(method)

close all;
load('dataset/CBcalib.mat');
cp=CBcalib{1}{1};
% clear all;
X=[0,0,0;
   0,10,0;
   0,20,0;
   1000, 0, 0;
   1000, 10, 0;
   1000, 20, 0];

x = cp.k*[cp.Rm cp.Tm] * [X ones(size(X,1),1)]';
x=x';
% x=[randn(3,1)*10+300 randn(3,1)*10+550;randn(3,1)*10+1700 randn(3,1)*10+550];

[cparams FBCerr FBCtime] = PoseEstimate(x, X, cp.k, cp.d, method)

% rp1 = f{1}{1}.rperror
% rp2 = f{1}{2}.rperror
% bard=[rp1;rp2]';
% 
% %plot rp errors comparitive
% createRPplot(bard, [0 11], [ones(1,2).*mean(rp1)], [ones(1,2).*mean(rp2)]);
% 
% for i=1:length(f{1}{1}.cparams)
%     c1(:,i)=getangle(f{1}{1}.cparams{i}.Rt);
%     c2(:,i)=getangle(f{1}{2}.cparams{i}.Rt);
%     d1(:,i)=(f{1}{1}.cparams{i}.Tt);
%     d2(:,i)=(f{1}{2}.cparams{i}.Tt);
% end
% mc1=mean(c1,2);
% mc2=mean(c2,2);
% maxDev1_R= sum(abs(c1-repmat(mean(c1,2),1,10)));
% maxDev2_R= sum(abs(c2-repmat(mean(c2,2),1,10)));
% 
% md1=mean(d1,2);
% md2=mean(d2,2);
% maxDev1_T= sum(abs(d1-repmat(mean(d1,2),1,10)));
% maxDev2_T= sum(abs(d2-repmat(mean(d2,2),1,10)));
% 
% 
% figure,
% plot(c1(1,:));hold on;
% plot(c1(2,:),'r');
% plot(c1(3,:),'g');
% axis([1 10 -50 10]);
% line([1,10],[mc1(1) mc1(1)],'Color',[0 0 1], 'LineStyle','--');
% line([1,10],[mc1(2) mc1(2)],'Color',[1 0 0], 'LineStyle','--');
% line([1,10],[mc1(3) mc1(3)],'Color',[0 1 0], 'LineStyle','--');
% hold off;
% 
% figure,
% plot(c2(1,:));hold on;
% plot(c2(2,:),'r');
% plot(c2(3,:),'g');
% axis([1 10 -50 10]);
% line([1,10],[mc2(1) mc2(1)],'Color',[0 0 1], 'LineStyle','--');
% line([1,10],[mc2(2) mc2(2)],'Color',[1 0 0], 'LineStyle','--');
% line([1,10],[mc2(3) mc2(3)],'Color',[0 1 0], 'LineStyle','--');
% hold off;
% 
% figure,
% plot(d1(1,:));hold on;
% plot(d1(2,:),'r');
% plot(d1(3,:),'g');
% axis([1 10 -500 3000]);
% line([1,10],[md1(1) md1(1)],'Color',[0 0 1], 'LineStyle','--');
% line([1,10],[md1(2) md1(2)],'Color',[1 0 0], 'LineStyle','--');
% line([1,10],[md1(3) md1(3)],'Color',[0 1 0], 'LineStyle','--');
% hold off;
% 
% figure,
% plot(d2(1,:));hold on;
% plot(d2(2,:),'r');
% plot(d2(3,:),'g');
% axis([1 10 -500 3000]);
% line([1,10],[md2(1) md2(1)],'Color',[0 0 1], 'LineStyle','--');
% line([1,10],[md2(2) md2(2)],'Color',[1 0 0], 'LineStyle','--');
% line([1,10],[md2(3) md2(3)],'Color',[0 1 0], 'LineStyle','--');
% hold off;
% 
% % 
% % figure,
% % bard=[rp1;rp2]
% % bar(bard)
% % % hold on;bar(repcam{2},'r');hold off;
% % axis([0 11 0 6])
% % 
% %  m1=mean(rp1)
% %  s1=std(rp2)
% %  m2=mean(rp2)
% %  s2=std(rp2)
% % 
% %  line([0, 11],[m1,m1],'LineStyle','--'); 
% %  line([0,11],[m2,m2],'Color',[1.0,0,0],'LineStyle','--');
% 
