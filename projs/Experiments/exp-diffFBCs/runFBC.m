function [ fbcparams FBCerr FBCtime ] = runFBC(pmethod)
%RUNFBC Summary of this function goes here
%   Detailed explanation goes here

% function [fbcparams FBCerr FBCtime] = PoseEstimate(ipts, ipts3d, k, d, pmethod)

load('dataset/database/data.mat')    

X=data.X;
x=data.x;
k=data.K;
d=data.D;

if length(x)~=length(X)
    'dimensions of inputs does not match'
    return
end

% undistort the image coordinates with known distortion coefficients.
npts = [undistortPts(x, d,  k)]';
npts3d = X';
size(npts)
size(npts3d)

% pose estimation based on pmethod
    switch(pmethod)
        case 'epfl'
            tic;
            [R,T,Xc,best_solution]=efficient_pnp(npts3d,npts, k); 
            et=toc;
        
        case 'epflgauss'
            tic;
            [R,T,Xc,best_solution]=efficient_pnp_gauss(npts3d,npts, k);     
            et=toc;
        
        case 'boug'
            fc=[ k(1,1); k(1,1)];
            cc=[ k(1,3); k(2,3)];
            tic;
            [R,T]=compute_extrinsic(npts',npts3d',fc,cc);
            et=toc;
             R= rodrigues(R);
       
        case 'gene'
            tic;
            [R,T]=estimate_extrinsic_traditional(npts3d,npts, k);
            et=toc;
            
        case 'zhang'
            tic;
            [K, R, T] = CalibZhang(npts,npts3d(:,1:2), 1, k);
            et=toc;
            R
            R=Rodrigues(R);
            
%             [knew, dnew, R, T, rperr] = RefineCamParam2(npts, npts3d(:,1:2),  k,  d, Ri, Ti);
%             FBCerr=rperr;
%             npts_2 = undistortPts(npts',dnew, knew);
%             [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts_2',R,T,knew);
            
        case 'tsai'
            'not implemented'
            return
%             tic;
%             npts=[npts ones(length(npts),1)];
%             size(npts)
%             size(npts3d)
%             if (sum(npts3d(:,3))==0)
%                npts3d(:,3) = ones(1,length(npts3d));
%             else
%                npts3d=npts3d./npts3d(end,:); 
%             end
%             [K, R, T, rperr] = CalibTsai(npts, npts3d, k);
%             rperr
%             R
%             T
%             et=toc;

        otherwise
            'not implemented'
            return
    end

%     [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T, k);
    FBCerr = CalibError(npts3d, npts, R, T,  k);

    %     refinment step
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d,  k,  d, R, T);
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam2(npts, npts3d(:,1:2),  k,  d, R, T);
%     [Knew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d,  k, R, T)
    fbcparams.k =  k;
    fbcparams.d =  d;
    fbcparams.R = R;
    fbcparams.T = T;
%     cparams.Rt = R;
%     cparams.Tt = T;
%     cparams.Rm = R;
%     cparams.Tm = T;
%     cparams.Rl = R;
%     cparams.Tl = T;
    
    FBCtime=et;

end

