%%
function xout = applyDistortion(xin, K, dist)

%% Add distortion:

[m,n] = size(xin);
if m~=2
    'X must be 2xN matrix'
    return;
end

% Pixel coordinates:
f=[K(1,1);K(2,2)];
c=[K(1,3);K(2,3)];

if length(f)>1,
    x = (xin - c*ones(1,n)) ./ (f * ones(1,n))  ;
    
else
    x = (xin - c*ones(1,n))/f;
end;

r2 = x(1,:).^2 + x(2,:).^2;
r4 = r2.^2;

% Radial distortion:
cdist = 1 + dist(1) * r2 + dist(2) * r4
xd1 = x .* (ones(2,1)*cdist);

% tangential distortion:
a1 = 2.*x(1,:).*x(2,:);
a2 = r2 + 2*x(1,:).^2;
a3 = r2 + 2*x(2,:).^2;

delta_x = [dist(3)*a1 + dist(4)*a2 ;
   dist(3) * a3 + dist(4)*a1]


xd2 = xd1 + delta_x;

% Pixel coordinates:
f=[K(1,1);K(2,2)];
c=[K(1,3);K(2,3)];

if length(f)>1,
    xp = xd2 .* (f * ones(1,n))  +  c*ones(1,n);
    
else
    xp = f * xd2 + c*ones(1,n);
end;

xout=xp;
end
