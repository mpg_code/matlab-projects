function xp=project3Dto2D(X, R, T, K, dist)
% expect X as 3 times N

[m,n] = size(X);
if m~=3
    'X must be 3xN matrix'
    return;
end
xcam = R*X + repmat(T,[1 n])

inv_Zcomp = 1./xcam(3,:)

x = (xcam(1:2,:) .* repmat(inv_Zcomp,2,1)) 

% Add sitortion
%xd2 = applyDistortion(x, dist);

% Pixel coordinates:
f=[K(1,1);K(2,2)];
c=[K(1,3);K(2,3)];

if length(f)>1,
    xp = xd2 .* (f * ones(1,n))  +  c*ones(1,n);
    
else
    xp = f * xd2 + c*ones(1,n);
end;


end


