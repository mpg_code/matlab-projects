function [div_r div_t]=fig2(m)

    load('dataset/CBcalib.mat');

    r=1;
    for j=1:length(m.fbc)
        for p = 1: length(m.fbc{j}.params)
            for cam =1:length(m.fbc{j}.params{p})
                %%diference in calib parameters
                fr=Rodrigues(m.fbc{j}.params{p}{cam}.Rm)*180/pi;
                cr=Rodrigues(CBcalib{1}{cam}.Rm)*180/pi;
                %camera - x, y, z - cbcW - y, x, z - FBCW - z, y, x
                rdiff(:,r) = abs([cr(2)+90;cr(1)+90;cr(3)]-[fr(3);fr(2);fr(1)]); 
                ft=m.fbc{j}.params{p}{cam}.Tm;
                ct=CBcalib{1}{cam}.Tm;
%                     tdiff(:,r) = [ct(1);ct(2);ct(3)]-[ft(2);ft(3);ft(1)]; 
                tdiff(:,r) = abs([ct-ft]); 
                r=r+1;
            end
        end
        div_r(:,j) = mean(rdiff,2);
        div_t(:,j) = mean(tdiff,2);
    end


    method = m.Algo;
    
    figure1=figure;
    axes1 = axes('Parent',figure1,...
    'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
    'XTick',[1 2 3]);
    box(axes1,'on');
    hold(axes1,'all');
    plot1=plot(div_r);
    for i = 1:length(method)
        set(plot1(i),'DisplayName',strcat('FBC-',method{i}));
    end
    title('Angular Difference FBC Vs CBC');
    xlabel('Rotational Axes');
    ylabel('Absolute Difference in Degrees');
    legend(axes1,'show'); 

 figure2=figure;
    axes2 = axes('Parent',figure2,...
    'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
    'XTick',[1 2 3]);
    box(axes2,'on');
    hold(axes2,'all');
    plot2=plot(div_t);
    for i = 1:length(method)
        set(plot2(i),'DisplayName',strcat('FBC-',method{i}));
    end
    title('Translation Difference FBC Vs CBC');
    xlabel('Tranlation Axes');
    ylabel('Absolute Difference in Millimeters');
    legend(axes1,'show'); 

   
% r=[T{1}; T{2}; T{3}; T{4}];
    figure3=figure;
%     'XTickLabel',{'epfl','bout','zhang','general'},...
    r=[];
    for i=1:length(method)
        r=[r;m.fbc{i}.time];
    end
    axes3 = axes('Parent',figure3,...
    'XTickLabel',method,...
    'XTick',[1:length(method)]);
    box(axes3,'on');
    hold(axes3,'all');
    bar(r);
     title('Execution time');
    xlabel('Algorithms');
    ylabel('Time in seconds');

   
%     % r=[T{1}; T{2}; T{3}; T{4}];
%     figure4=figure;
% %     'XTickLabel',{'epfl','bout','zhang','general'},...
%     e=[];
%     for i=1:length(method)
%         e=[e;m.fbc{i}.error.rp];
%     end
%     axes4 = axes('Parent',figure4,...
%     'XTickLabel',method,...
%     'XTick',[1:length(method)]);
%     box(axes4,'on');
%     hold(axes4,'all');
%     bar(e);
%      title('Calbration 2D Error');
%     xlabel('Algorithms');
%     ylabel('Mean Squared Pixel Error');
% 
% % r=[T{1}; T{2}; T{3}; T{4}];
%     figure5=figure;
% %     'XTickLabel',{'epfl','bout','zhang','general'},...
%     e2=[];
%     for i=1:length(method)
%         e2=[e2;m.fbc{i}.error.rmse];
%     end
%     axes5 = axes('Parent',figure5,...
%     'XTickLabel',method,...
%     'XTick',[1:length(method)]);
%     box(axes5,'on');
%     hold(axes5,'all');
%     bar(e2);
%      title('Calbration 3D Error');
%     xlabel('Algorithms');
%     ylabel('Root Mean Squared 3D Error');

end