 
function Measurements = expFBCvsCBObsFbc(sch, obs, fbcpts)

Measurements = [];
% clear all;
% close all;

 % load all scanner calibration data
    load('dataset/CBcalib.mat');
  
    % load reference measurements
    load ('dataset/pantoref.mat')

    % load observation measurement
%     load ('dataset/obs.mat')
%     load ('dataset/obsU.mat')
%     obs=obsU;

%         load fbc points
%         load ('fbc-points/fbcptsU.mat')
%         fbcpts = fbcptsU;


%     if(~iscell(method) & method=='all')
        method={'epfl','boug','zhang','gold'};
%     end
%     method={'epfl','boug','zhang','gene'};
    for m=1:length(method)
        switch(sch)
           case 1
           [w d wc dc fbc] = scheme1(method{m},CBcalib, pantoref, obs, fbcpts);
           case 2
           [w d wc dc fbc] = scheme2(method{m},CBcalib, pantoref, obs, fbcpts);
        end
%         W{m}=w;
%         D{m}=d;
%         WC{m}=wc;
%         DC{m}=dc;
%         T{m}=time;
%         E{m}=error;
        Measurements.W{m}=w;
        Measurements.D{m}=d;
        Measurements.WC{m}=wc;
        Measurements.DC{m}=dc;
        Measurements.fbc{m}=fbc;
        Measurements.Algo{m}=method{m};

    end
%     figplot(Measurements,method);
%     figplot(W,D,WC,DC,T,E,method);
end

function [w d wc dc fbc] = scheme2(method, CBcalib, pantoref, obs, fbcpts)

    Nr=30;
    wc=[];
    dc=[];
    w=[];
    d=[];
    for r=1:Nr
           rng=randperm(10);
           img=rng(1);
            [f fe ft]=fbc_main(method,[img], fbcpts);
            fbc.params(r) = f;
            fbc.time(r)=ft;
            fbc.error(r)=fe;

            [wct dct ect ert]=panto('fbc', f, pantoref,obs);
            [wt dt et rt]=panto('cb', CBcalib, pantoref,obs);

            if(isempty(wc)||isempty(w)||isempty(dc)||isempty(d))
                wc=wct;
                w=wt;
                dc=dct;
                d=dt;
            else
                wc=wc+wct;
                w=w+wt;
                dc=dc+dct;
                d=d+dt;
            end
            
%             figplot(w,d,wc,dc);
%             pause;
    end
    wc=wc./Nr;
    w=w./Nr;
    dc=dc./Nr;
    d=d./Nr;
     
%     [mw md mwc mdc mVal] = figplot(w,d,wc,dc);
   
    
%     mean(reshape(w,4,10),2)
%     mean(reshape(d,4,10),2)
%     
%     mean(reshape(wc,4,10),2)
%     mean(reshape(dc,4,10),2)


end


function [w d wc dc fbc] = scheme1(method, CBcalib, pantoref, obs, fbcpts)

    Nr=10;%fixed number to obs number
    wc=[];
    dc=[];
    w=[];
    d=[];
    for r=1:Nr
%         if r==3
%             continue;
%         end
           img=r;
            [f fe ft]=fbc_main(method,[img], fbcpts);
            fbc.params(r) = f;
            fbc.time(r)=ft;
            fbc.error(r)=fe;

            [wct dct ect ert]=panto('fbc', f, pantoref,obs);
            [wt dt et rt]=panto('cb', CBcalib, pantoref,obs);

            st = (r-1)*4+1;
            if(isempty(wc)||isempty(w)||isempty(dc)||isempty(d))
                wc=wct(st:st+3);
                w=wt(st:st+3);
                dc=dct(st:st+3);
                d=dt(st:st+3);
            else
                wc=[wc wct(st:st+3)];
                w=[w wt(st:st+3)];
                dc=[dc dct(st:st+3)];
                d=[d dt(st:st+3)];
            end
    end
%     wc=wc./Nr;
%     w=w./Nr;
%     dc=dc./Nr;
%     d=d./Nr;
    
     
%     [mw md mwc mdc mVal] = figplot(w,d,wc,dc);
   
    
%     mean(reshape(w,4,10),2)
%     mean(reshape(d,4,10),2)
%     
%     mean(reshape(wc,4,10),2)
%     mean(reshape(dc,4,10),2)


end



function FBCcalib = getFBCcalib(FullFBCcalib, pos)
    for scn = 1:length(FullFBCcalib) % loop over the scanner
        for cam=1:length(FullFBCcalib{scn}) % loop over the cameras in a scanner
            FBCcalib{scn}{cam}=FullFBCcalib{scn}{cam}.cparams{pos};
        end
    end
end
