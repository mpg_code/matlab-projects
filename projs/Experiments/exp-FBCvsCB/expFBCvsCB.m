 
function [mw md mwc mdc mVal] = expFBCvsCB(method, sch)

% clear all;
close all;

 % load all scanner calibration data
    load('dataset/CBcalib.mat');
  
    % load reference measurements
    load ('dataset/pantoref.mat')

    % load observation measurement
%     load ('dataset/obs.mat')
      load ('dataset/obsU.mat')
    obs=obsU;

    
    %         load fbc points
        load ('fbc-points/fbcptsU.mat')
        fbcpts = fbcptsU;
        
%     [f fe ce]=fbc_main(method,[2]);
%     
% %     randomized selection
% % for rep=1:100
% %     rng=randperm(length(f{1}{1}.cparams));
% %     newfbc = getFBCcalib(f,rng(1));
% %     [wc dc]=panto('fbc', CBcalib, newfbc, pantoref,obs);
% %     [w d]=panto('cb', CBcalib, newfbc, pantoref,obs);
% %     mw(:,rep) = mean(reshape(w,4,10),2);
% %     md(:,rep) = mean(reshape(d,4,10),2);
% % 
% %     mwc(:,rep) = mean(reshape(wc,4,10),2);
% %     mdc(:,rep) = mean(reshape(dc,4,10),2);
% % end
%     
%for all fbccalibs

        
%         newfbc{1}{1}.Rt
%         newfbc{1}{2}.Rt
   switch(sch)
       case 1
       [w d wc dc time] = scheme1(method,CBcalib, pantoref, obs, fbcpts);
       case 2
       [w d wc dc time] = scheme2(method,CBcalib, pantoref, obs, fbcpts);
   end
   [mw md mwc mdc mVal] = figplot(w,d,wc,dc,time);
end

% function [mw md mwc mdc mVal] = scheme1(method, CBcalib, pantoref, obs)
function [w d wc dc time] = scheme2(method, CBcalib, pantoref, obs, fbcpts)

    Nr=30;
    wc=[];
    dc=[];
    w=[];
    d=[];
    for r=1:Nr
           rng=randperm(10);
           img=rng(1)
            [f fe ft]=fbc_main(method,[img], fbcpts);
            time(r)=ft;

           [wct dct ect ert]=panto('fbc', f, pantoref,obs);
            [wt dt et rt]=panto('cb', CBcalib, pantoref,obs);

            if(isempty(wc)||isempty(w)||isempty(dc)||isempty(d))
                wc=wct;
                w=wt;
                dc=dct;
                d=dt;
            else
                wc=wc+wct;
                w=w+wt;
                dc=dc+dct;
                d=d+dt;
            end
%             figplot(w,d,wc,dc);
%             pause;
    end
    wc=wc./Nr;
    w=w./Nr;
    dc=dc./Nr;
    d=d./Nr;
    
     
%     [mw md mwc mdc mVal] = figplot(w,d,wc,dc);
   
    
%     mean(reshape(w,4,10),2)
%     mean(reshape(d,4,10),2)
%     
%     mean(reshape(wc,4,10),2)
%     mean(reshape(dc,4,10),2)


end


% function [mw md mwc mdc mVal] = scheme2(method, CBcalib, pantoref, obs)

function [w d wc dc time] = scheme1(method, CBcalib, pantoref, obs, fbcpts)
    Nr=10;%fixed number to obs number
    wc=[];
    dc=[];
    w=[];
    d=[];
    for r=1:Nr
%         if r==3
%             continue;
%         end
           img=r
            [f fe ft]=fbc_main(method,[img], fbcpts);
            time(r)=ft;

            [wct dct ect ert]=panto('fbc', f, pantoref,obs);
            [wt dt et rt]=panto('cb', CBcalib, pantoref,obs);

            st = (r-1)*4+1;
            if(isempty(wc)||isempty(w)||isempty(dc)||isempty(d))
                wc=wct(st:st+3);
                w=wt(st:st+3);
                dc=dct(st:st+3);
                d=dt(st:st+3);
            else
                wc=[wc wct(st:st+3)];
                w=[w wt(st:st+3)];
                dc=[dc dct(st:st+3)];
                d=[d dt(st:st+3)];
            end
    end
%     wc=wc./Nr;
%     w=w./Nr;
%     dc=dc./Nr;
%     d=d./Nr;
    
     
%     [mw md mwc mdc mVal] = figplot(w,d,wc,dc);
   
    
%     mean(reshape(w,4,10),2)
%     mean(reshape(d,4,10),2)
%     
%     mean(reshape(wc,4,10),2)
%     mean(reshape(dc,4,10),2)


end

function [mw md mwc mdc mVal] = figplot(w,d,wc,dc,T)

length(w)
length(wc)
length(d)
length(dc)
assert(length(w)~=length(wc)~=length(d)~=length(dc));

    wi = reshape(w,4,length(w)/4);
    di = reshape(d,4,length(d)/4);
    wci = reshape(wc,4,length(wc)/4);
    dci = reshape(dc,4,length(dc)/4);

    mw = mean(reshape(w,4,length(w)/4),2)
    md = mean(reshape(d,4,length(d)/4),2);
    mwc = mean(reshape(wc,4,length(wc)/4),2);
    mdc = mean(reshape(dc,4,length(dc)/4),2);
    
%         figure(1), bar([mean(mw) mean(mwc)]);
%     figure(2), bar([mean(md) mean(mdc)]);
%     figure, plot(w);hold on ; plot(wc,'r');hold off;
% 
%     figure, plot(d);hold on ; plot(dc,'r');hold off;

    figure1 = figure;
   axes1 = axes('Parent',figure1,...
    'XTickLabel',{'Vertical crack','Carbon missing','Edge crack','Abnormal'},...
    'XTick',[1 2 3 4]);
    box(axes1,'on');
    hold(axes1,'all');
    bar1=bar([(mw) (mwc)]);
    set(bar1(1),'DisplayName','CBcalib');
    set(bar1(2),'DisplayName','FBcalib');
    title('Width Error');
    xlabel('Defects');
    ylabel('Error in mm');
    legend(axes1,'show');

%     for i=1:4
%         figure, plot(wi(i,:));hold on ; plot(wci(i,:),'r');hold off;
%     end
    
    figure2 = figure;
   axes2 = axes('Parent',figure2,...
    'XTickLabel',{'Vertical crack','Carbon missing','Edge crack','Abnormal'},...
    'XTick',[1 2 3 4]);
    box(axes2,'on');
    hold(axes2,'all');
    bar2=bar([(md) (mdc)]);
    set(bar2(1),'DisplayName','CBcalib');
    set(bar2(2),'DisplayName','FBcalib');
    title('Depth Error');
    xlabel('Defects');
    ylabel('Error in mm');
    legend(axes2,'show');
%     for i=1:4
%         figure, plot(di(i,:));hold on ; plot(dci(i,:),'r');hold off;
%         ymax=ceil(round(max(di(i,:)))/5)*5;
%         axis([1 10 0 ymax])
%     end

    mVal = [mean(mw); mean(mwc); mean(md); mean(mdc)];

    r=T;
    figure3=figure;
    bar(r);
    title('Execution time');
    xlabel('Algorithm');
    ylabel('Time in seconds');
    
end

function FBCcalib = getFBCcalib(FullFBCcalib, pos)
    for scn = 1:length(FullFBCcalib) % loop over the scanner
        for cam=1:length(FullFBCcalib{scn}) % loop over the cameras in a scanner
            FBCcalib{scn}{cam}=FullFBCcalib{scn}{cam}.cparams{pos};
        end
    end
end
