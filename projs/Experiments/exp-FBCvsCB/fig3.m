function [d1 d2 d3] = fig3(m, type)

% samples=40;
%     bin=10;
% id=[samples zeros(1,bin-1)]';
% % width
%     n=hist([m.W{1}],bin)';
%     KLDW(1) = ChiSquare(id, n);    
% %   KLDW(1) = KLDiv(id', n');    
% %     n=[n(end:-1:2) n];
%  %     pn= n./length(n);
%     for a = 1:length(m.Algo)
%         nn(:,a) =hist([m.WC{a}],bin);
% %         nn(:,a)=[intw(end:-1:2) intw];
%         KLDW(a+1) = ChiSquare(id, nn(:,a));    
%   
% %         pnn(:,a)= nn(:,a)./length(nn(:,a));
% %         KLDW(a) = KLDiv(pn, pnn(:,a)');    
% %         KLDW(a+1) = KLDiv(id', nn(:,a)');    
%     end
% %   KLDW(a+1) = ChiSquare(id, n);
% 
% % depth
%     nd=hist([m.D{1}],bin)';
% %     nd=[nd(end:-1:2) nd];
%     KLDD(1) = ChiSquare(id, nd);
%     
% %   KLDD(1) = KLDiv(id', nd');    
% 
% %     pnd= nd./length(nd);
%     for a = 1:length(m.Algo)
%         nnd(:,a) =hist([m.DC{a}],bin);
% %         nnd(:,a)=[intd(end:-1:2) intd];
%         KLDD(a+1) = ChiSquare(id, nnd(:,a));    
% %           KLDD(a+1) = KLDiv(id', nnd(:,a)');    
% 
% %         pnnd(:,a)= nnd(:,a)./length(nnd(:,a));
% %         KLDD(a) = KLDiv(pnd, pnnd(:,a)');     
% %         KLDD(a) = KLDiv(nd, nnd(:,a)'); 
%     end
% 
% % both width adn depth
%     nt=hist([m.W{1} m.D{1}],bin)';
% %     nt=[nt(end:-1:2) nt];
%     
% idt=[samples*2 zeros(1,bin-1)]';
% KLDT(1) = ChiSquare(idt, nt);    
% %   KLDT(1) = KLDiv(idt', nt');    
% 
% %     pnt= nt./length(nt);
% 
%     for a = 1:length(m.Algo)
% m.Algo(a)
%         nnt(:,a)=hist([m.WC{a} m.DC{a}],bin)   
% %         nnt(:,a)=[intt(end:-1:2) intt];
%         KLDT(a+1) = ChiSquare(id, nnt(:,a));    
% %         KLDT(a+1) = KLDiv(idt', nnt(:,a)');    
% 
%   
% %         pnnt(:,a)= nnt(:,a)./length(nnt(:,a));
% %         KLDT(a) = KLDiv(pnt, pnnt(:,a)');
% %         KLDT(a) = KLDiv(nt, nnt(:,a)');    
%     end

[distance error] = CompareHistogram(m, type)
d1=distance{1};
d2=distance{2};
d3=distance{3};


if(isempty(error)) 
    return;
end
 method = m.Algo;
  
  
    figure1=figure;
    axes1 = axes('Parent',figure1);
% ,...
%     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
%     'XTick',[1 2 3]);
    box(axes1,'on');
    hold(axes1,'all');
% [id nt nnt]
%     plot1=plot([idt nt nnt]);
    plot1=plot(error{3});
    set(plot1(1),'DisplayName','Ideal','LineStyle','-', 'LineWidth',2);
    set(plot1(2),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
    for i = 1:length(method)
        set(plot1(i+2),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
    end
%     title('Discrete Probability Distribution');
    xlabel('Sample bins');
    ylabel('Error PDF');
    legend(axes1,'show'); 

%     figure1=figure;
%     axes1 = axes('Parent',figure1);
% % ,...
% %     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
% %     'XTick',[1 2 3]);
%     box(axes1,'on');
%     hold(axes1,'all');
%     plot1=plot([n' nn]);
%     set(plot1(1),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
%     for i = 1:length(method)
%         set(plot1(i+1),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
%     end
%     title('Discrete Probability Distribution');
%     xlabel('Sample bins');
%     ylabel('Width Error Distribution');
%     legend(axes1,'show'); 
% 
%   figure2=figure;
%     axes2 = axes('Parent',figure2);
% % ,...
% %     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
% %     'XTick',[1 2 3]);
%     box(axes2,'on');
%     hold(axes2,'all');
%     plot2=plot([nd' nnd]);
%     set(plot2(1),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
%     for i = 1:length(method)
%         set(plot2(i+1),'DisplayName',strcat('FBC-',method{i}),'LineStyle','--', 'LineWidth',1);
%     end
%     title('Discrete Probability Distribution');
%     xlabel('Sample bins');
%     ylabel('Depth Error Distribution');
%     legend(axes1,'show'); 


    errcdf = [zeros(1,size(error{3},2)); error{3}];
    for s = 2:size(errcdf,1)
        errcdf(s,:) = errcdf(s-1,:) + errcdf(s,:);    
    end
    
     figure2=figure;
    axes2 = axes('Parent',figure2);
% ,...
%     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
%     'XTick',[1 2 3]);
    box(axes2,'on');
    hold(axes2,'all');
% [id nt nnt]
%     plot1=plot([idt nt nnt]);
    plot2=plot([0:2:20],errcdf);
    set(plot2(1),'DisplayName','Ideal','LineStyle','-', 'LineWidth',2);
    set(plot2(2),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
    for i = 1:length(method)
        set(plot2(i+2),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
    end
%     title('Discrete Probability Distribution');
    xlabel('Total error in millimeters');
    ylabel('Cumulative Density Function');
    legend(axes2,'show'); 
    axis([-inf inf 0 1.2])
end

