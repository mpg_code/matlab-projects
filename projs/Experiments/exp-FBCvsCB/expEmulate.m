 
function [wres dres tres]= expEmulate(sch,emu)

switch(emu)
    case 'pixel'
        emuxlabel='Pixel Noise (variance)'
        axisrange = [-inf inf 0 2.0];
    case 'uplift'
        emuxlabel='Uplift (mm)'
        axisrange = [-inf inf 0 2.0];
    case'yaw'
        emuxlabel='Yaw Angle (degrees)'
        axisrange = [-inf inf 0 2.0];
    case 'roll'
        emuxlabel='Roll Angle (degrees)'
        axisrange = [-inf inf 0 2.0];
    case 'pitch'
        emuxlabel='Pitch Angle (degrees)'
        axisrange = [-inf inf 0 2.0];
    otherwise
        'not implemented'
        return
 end
% clear all;
close all;

%     %load Calibration
%    load('dataset/CBcalib.mat');
  
    % load observation measurement
%     load ('dataset/obs.mat')
    load ('dataset/obsU.mat')
    obs=obsU;
    
%       load fbc points
        load ('fbc-points/fbcptsU.mat')
        fbcpts = fbcptsU;

     %%emulate
%     emu = {'pixel', 'uplift', 'yaw', 'roll', 'tilt'};
        
%     loop over various emulations
%     for e=1:length(emu)
        [obsArr fbcptsArr rangeArr] = emulateError(obs, fbcpts, emu);
%         loop over range of emulated observation set.
        wres = [];
        dres = [];
        tres = [];
        for t=1:length(rangeArr)
            'loop' 
            t
            obs=obsArr{t};
            fbcpts=fbcptsArr{t};
            m = expFBCvsCBObsFbc(sch, obs, fbcpts);
            [dist error] = CompareHistogram(m,'kld');
            wres=[wres;dist{1}];
            dres=[dres;dist{2}];
            tres=[tres;dist{3}];
        end

    method={'epfl','boug','zhang','gold'};

 figure1=figure;
    axes1 = axes('Parent',figure1);
% ,...
%     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
%     'XTick',[1 2 3]);
    box(axes1,'on');
    hold(axes1,'all');
    plot1=plot(rangeArr, wres);
    set(plot1(1),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
    for i = 1:length(method)
        set(plot1(i+1),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
    end
%     title(emutitle);
    xlabel(emuxlabel);
    ylabel('Width Error (KLD)');
    legend(axes1,'show'); 
    axis(axisrange);

     figure2=figure;
    axes2 = axes('Parent',figure2);
% ,...
%     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
%     'XTick',[1 2 3]);
    box(axes2,'on');
    hold(axes2,'all');
    plot2=plot(rangeArr, dres);
    set(plot2(1),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
    for i = 1:length(method)
        set(plot2(i+1),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
    end
%     title(emutitle);
    xlabel(emuxlabel);
    ylabel('Depth Error (KLD)');
    legend(axes2,'show'); 
    axis(axisrange);
 

figure3=figure;
    axes3 = axes('Parent',figure3);
% ,...
%     'XTickLabel',{'X axis', 'Y axis', 'Z axis'},...
%     'XTick',[1 2 3]);
    box(axes3,'on');
    hold(axes3,'all');
    plot3=plot(rangeArr, tres);
    set(plot3(1),'DisplayName','CBC','LineStyle','-', 'LineWidth',1.5);
    for i = 1:length(method)
        set(plot3(i+1),'DisplayName',strcat('FBC-',method{i}),'LineStyle','-.', 'LineWidth',1.25);
    end
%     title(emutitle);
    xlabel(emuxlabel);
    ylabel('Width and Depth Error (KLD)');
    legend(axes3,'show'); 
    axis(axisrange);
end