function FBCcalib = fbc_minRperr()

% run this only for generating one set of FBC calib paprameters
% that gives lowest possible RP
FullFBCcalib = fbc_main('epfl');
for scn = 1:length(FullFBCcalib) % loop over the scanner
    for cam=1:length(FullFBCcalib{scn}) % loop over the cameras in a scanner
        [val pos]=min(FullFBCcalib{scn}{cam}.rperror);
        pos=length(FullFBCcalib{scn}{cam}.rperror);
        FBCcalib{scn}{cam}=FullFBCcalib{scn}{cam}.cparams{pos};
    end
end

save('fbc_panto/FBCcalib.mat','FBCcalib');
% 
% % creation of data - run the following
% % ArrangeScannerData(1,'Gwest',f,g,h);
% % ArrangeObsrevationData(o,meta);
% % ArrangeReferenceMeas(raw);
% % 
% % load all scanner calibration data
% load('CBcalib.mat');
% 
% % load features for calibration
% load ('fbcpts.mat')
% 
% 
% for scn = 1:length(fbcpts) % loop over the scanner
%     for cam=1:length(fbcpts{scn}) % loop over the cameras in a scanner
%         ipts = fbcpts{scn}{cam}.FeatPts;
%         ipts3d = fbcpts{scn}{cam}.RefPts;
%         cp = CBcalib{scn}{cam};
%         rpe=[];
%         for i=1:length(ipts)
%             [err cparams] = fbcAlgorithm(i,ipts,ipts3d,cp,'epfl');
%             rpe(i)=err;
%             fbccalibI{i}=cparams;
%         end
%         [val pos]=min(rpe);
%         rpe;
% %         repcam{cam}=rpe;
% %         mean(repcam{cam})
% %         std(repcam{cam})
%         fbccalibI{length(ipts)}.Rt
%         cparams.Rt
%         FBCcalib{scn}{cam}=cparams;        
% %         FBCcalib{scn}{cam}=fbccalibI{pos};        
%     end
% end
% 
% save('fbc_panto/FBCcalib.mat','FBCcalib');
% 
% %     
%     x=1:10;
%     length(repcam{1})
%     mean(repcam{1})
%     e=std(repcam{1})*ones(10,1)
%     figure, errorbar(x,repcam{1},e)