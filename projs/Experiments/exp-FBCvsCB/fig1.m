
% figure of width error, depth error and execition time for 
% various algorithms
% for given observation of 40 smaples

function oi= fig1(meas)
% function oi= figplot(W,D,WC,DC,T,E, method)

W=meas.W;
WC=meas.WC;
D=meas.D;
DC=meas.DC;
fbc=meas.fbc;
method=meas.Algo;

length(W)
length(WC)
length(D)
length(DC)
assert(length(W)~=length(WC)~=length(D)~=length(DC));

for l=1:length(method)
    w=W{l};
    wc=WC{l};
    d=D{l};
    dc=DC{l};
    
%     wi = reshape(w,4,length(w)/4);
%     di = reshape(d,4,length(d)/4);
%     wci = reshape(wc,4,length(wc)/4);
%     dci = reshape(dc,4,length(dc)/4);

    mw = mean(reshape(w,4,length(w)/4),2)
    md = mean(reshape(d,4,length(d)/4),2);
    mwc = mean(reshape(wc,4,length(wc)/4),2);
    mdc = mean(reshape(dc,4,length(dc)/4),2);
    
    MW{l}=mw;
    MD{l}=md;
    MWC{l}=mwc;
    MDC{l}=mdc;
end

% save MW MW
% save MD MD
% save MWC MWC
% save MDC MDC
%         figure(1), bar([mean(mw) mean(mwc)]);
%     figure(2), bar([mean(md) mean(mdc)]);
%     figure, plot(w);hold on ; plot(wc,'r');hold off;
% 
%     figure, plot(d);hold on ; plot(dc,'r');hold off;

    figure1 = figure;
    axes1 = axes('Parent',figure1,...
    'XTickLabel',{'Vertical crack','Carbon missing','Edge crack','Abnormal'},...
    'XTick',[1 2 3 4]);
    box(axes1,'on');
    hold(axes1,'all');
barfbc=[(MW{1})];
for i = 1:length(method)
    barfbc=[barfbc (MWC{i})];
end
    bar1=bar(barfbc);
ylim([0 10]);
%     bar1=bar([(MW{1}) (MWC{1}) (MWC{2}) (MWC{3}) (MWC{4})]);
    set(bar1(1),'DisplayName','CBC');
for i = 1:length(method)
    set(bar1(i+1),'DisplayName',strcat('FBC-',method{i}));
end
%     set(bar1(2),'DisplayName','FBC-epfl');
%     set(bar1(3),'DisplayName','FBC-boug');
%     set(bar1(4),'DisplayName','FBC-zhang');
%     set(bar1(5),'DisplayName','FBC-general');
%     title('Width Error');
    xlabel('Defects');
    ylabel('Error in mm');
    legend(axes1,'show'); 

%     for i=1:4
%         figure, plot(wi(i,:));hold on ; plot(wci(i,:),'r');hold off;
%     end
    
    figure2 = figure;
   axes2 = axes('Parent',figure2,...
    'XTickLabel',{'Vertical crack','Carbon missing','Edge crack','Abnormal'},...
    'XTick',[1 2 3 4]);
    box(axes2,'on');
    hold(axes2,'all');
barfbc=[(MD{1})];
for i = 1:length(method)
    barfbc=[barfbc (MDC{i})];
end
    bar2=bar(barfbc);
ylim([0 20]);
%     bar2=bar([(MD{1}) (MDC{1}) (MDC{2}) (MDC{3}) (MDC{4})]);
    set(bar2(1),'DisplayName','CBC');
for i = 1:length(method)
    set(bar2(i+1),'DisplayName',strcat('FBC-',method{i}));
end
%     set(bar2(2),'DisplayName','FBC-epfl');
%     set(bar2(3),'DisplayName','FBC-boug');
%     set(bar2(4),'DisplayName','FBC-zhang');
%     set(bar2(5),'DisplayName','FBC-general');
%     title('Depth Error');
    xlabel('Defects');
    ylabel('Error in mm');
    legend(axes2,'show');
%     for i=1:4
%         figure, plot(di(i,:));hold on ; plot(dci(i,:),'r');hold off;
%         ymax=ceil(round(max(di(i,:)))/5)*5;
%         axis([1 10 0 ymax])
%     end

%     mVal = [mean(mw); mean(mwc); mean(md); mean(mdc)];


end