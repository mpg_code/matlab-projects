function [obsE fbcptsE rangeE] = emulateError(obs, fbcpts, emu)


    %load Calibration
   load('dataset/CBcalib.mat');

pixelerrRange = [-10:2:10];
upliftRange = [500:-50:-500]; %low to high uplift -0.5mts to +0.5mts
yawRange = [-10:2:10];
rollRange=yawRange;
pitchRange = yawRange;

 switch(emu)
    case 'pixel'
        rangeE = pixelerrRange; 
    case 'uplift'
        rangeE = upliftRange;  
    case'yaw'
        rangeE = yawRange;  
    case 'roll'
        rangeE = rollRange;  
    case 'pitch'
        rangeE = pitchRange;  
     otherwise
        'not implemented'
        return
 end

 obsE=[];
 fbcptsE=[];
 for t=1:length(rangeE)
    obsint = obs;
    fbcptsint = fbcpts;
% emulating possible errors in the observation points
    for oind = 1:length(obs)
        dmeta = obs{oind}.meta;
        % Gather three image points
        ipts = obs{oind}.ImgPts;

        scanN = dmeta(1);
        camN = dmeta(2);
        % From metadata find which calibration data to use
        cp=CBcalib{scanN}{camN};


% special case for pixel error in 2D
if(strcmp(emu,'pixel'))
    nipts = ipts + rangeE(t)*randn();
    obsint{oind}.ImgPts = nipts;
    continue;
end

        % backproject to 3D
        dpts3D = BackProjectDefectPoints(ipts, cp);

        %introduce error due to various emulation
        newdpts3D = dpts3D;
        switch(emu)
                  
            case 'uplift'
                 for pro = 1:3 
                      % three points are converted to corresp. 3d using corresp. profile calib data.
                      X1 = dpts3D{pro}(:,1);
                      X2 = dpts3D{pro}(:,2);
                      X3 = dpts3D{pro}(:,3);
                      newdpts3D{pro}(1,1)= X1(1) + rangeE(t);
                      newdpts3D{pro}(1,2)= X2(1) + rangeE(t);
                      newdpts3D{pro}(1,3)= X3(1) + rangeE(t);
                 end
            case 'yaw'
                for pro = 1:3
                   newdpts3D{pro} = rodrigues([rangeE(t) 0 0].*pi./180) * (dpts3D{pro} + [-150 -150 -150; -540 -540 -540; 0 0 0]);
                end
            case 'roll'
                for pro = 1:3
                   newdpts3D{pro} = rodrigues([0 0 rangeE(t)].*pi./180) * (dpts3D{pro} + [-150 -150 -150; -540 -540 -540; 0 0 0]);
                end

            case 'pitch'
                for pro = 1:3
                   newdpts3D{pro} = rodrigues([0 rangeE(t) 0].*pi./180) * (dpts3D{pro} + [-150 -150 -150; -540 -540 -540; 0 0 0]);
                end

            otherwise
            'not implemented'
            return
        end

        %to 2d again
        obsint{oind}.ImgPts = ForwardProjectDefectPoints(newdpts3D, cp);
    end
    obsE{t}=obsint;

    % emulating possible errors in the fbc points
    for scn = 1:length(fbcpts) % loop over the scanner
        for cam=1:length(fbcpts{scn}) % loop over the cameras in a scanner
            ipts = fbcpts{scn}{cam}.FeatPts;
            cpf=CBcalib{scn}{cam};
            % gather data of all images in teh irange
            for in = 1: length(ipts)

% special case for pixel error in 2D
if(strcmp(emu,'pixel'))
    fbcptsint{scn}{cam}.FeatPts{in} = ipts{in} + rangeE(t)*randn();
    continue;
end

                 % backproject to 3D
                fpts3D = BackProjectDefectPoints(ipts{in}, cpf);
                 %introduce error due to various emulation
                newfpts3D = fpts3D;
                switch(emu)
                    case 'uplift'
                              % two points are converted to corresp. 3d using corresp. profile calib data.
                         for pro = 1:3 
                              X1 = fpts3D{pro}(:,1); 
                              X2 = fpts3D{pro}(:,2);
                              newfpts3D{pro}(1,1)= X1(1) + upliftRange(t);
                              newfpts3D{pro}(1,2)= X2(1) + upliftRange(t);
                         end
                    case 'yaw'
                        for pro = 1:3
                            newfpts3D{pro} = rodrigues([rangeE(t) 0 0].*pi./180) * (fpts3D{pro} + [-150 -150; -540 -540; 0 0]);
                        end

                    case 'roll'
                        for pro = 1:3
                            newfpts3D{pro} = rodrigues([0 0 rangeE(t)].*pi./180) * (fpts3D{pro} + [-150 -150; -540 -540; 0 0]);
                        end

                    case 'pitch'
                        for pro = 1:3
                            newfpts3D{pro} = rodrigues([0 rangeE(t) 0].*pi./180) * (fpts3D{pro} + [-150 -150; -540 -540; 0 0]);
                        end

                    otherwise
                    'not implemented'
                    return
                end

                 %to 2d again
                fbcptsint{scn}{cam}.FeatPts{in} = ForwardProjectDefectPoints(newfpts3D, cpf);
            end    
        end
    end
    fbcptsE{t}=fbcptsint;

 
 end
end