
function [R,T]=estimate_extrinsic_zhangplanar(npts3d,npts,A) 
%     npts3d=npts3d(:,1:2);
    [r,t] = extrinsicsPlanar(npts, npts3d(:,1:2), A);
%     [r,t] = extrinsicsNonPlanar(npts, npts3d, A);

    R=vision.internal.calibration.rodriguesVectorToMatrix(r);
    T=t';
end

function [R,T] = extrinsicsPlanar(imagePoints, worldPoints, intrinsics)

% Get inverse intrinsic matrix.
Ainv = inv(intrinsics);

% Compute homography.
H = fitgeotrans(worldPoints, imagePoints, 'projective');
H = H.T;
h1 = H(1, :);
h2 = H(2, :);
h3 = H(3, :);

lambda = 1 / norm(h1 * Ainv);%#ok

% Compute 3D rotation matrix.
r1 = lambda * h1 * Ainv;%#ok
r2 = lambda * h2 * Ainv;%#ok
r3 = cross(r1, r2);
R = [r1; r2; r3];

% R may not be a true rotation matrix because of noise in the data.
% Find the best rotation matrix to approximate R using SVD.
[U, ~, V] = svd(R);
R = U * V';

% Compute translation vector.
T = lambda * h3 * Ainv;%#ok

end

function [R,T] = extrinsicsNonPlanar(imagePoints, worldPoints, intrinsics)
% Based on [2] "Gold Standard Algorithm" on page 181

%make input points homogeneous
worldPoints = cat(2, worldPoints, ones(size(worldPoints,1),1));
imagePoints = cat(2, imagePoints, ones(size(imagePoints,1),1));

M = size(imagePoints, 1);

[imagePoints, Timage] = vision.internal.normalizePoints(imagePoints', 2, ...
    class(imagePoints));
imagePoints = imagePoints';

[worldPoints, Tworld] = vision.internal.normalizePoints(worldPoints', 3, ...
    class(worldPoints));
worldPoints = worldPoints';

% DLT
A = [zeros(4,M),                           worldPoints(:,1:4)';
    -worldPoints(:,1:4)',                       zeros(4,M);
 bsxfun(@times,worldPoints(:,1:4)', imagePoints(:,2)'), -bsxfun(@times,worldPoints(:,1:4)',imagePoints(:,1)')];
[U,~,~] = svd(A);
P = reshape(U(:,end),4,3);

% Denormalize
P = Tworld' * P / Timage';

% Extract extrinsics using intrinsics
RT = P / intrinsics;
R = RT(1:3, 1:3);
[U,S,V] = svd(R);
R = U*V';
T = RT(4,:)/S(1,1); % Scale translation vector using correction
end
