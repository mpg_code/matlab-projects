function [distance error bin] = CompareHistogram(m,type)

if (strcmp(type,'mean'))
    KLDW(1) = mean(m.W{1});
    KLDD(1) = mean(m.D{1});
    KLDT(1) = mean([m.W{1} m.D{1}]);
     
    for a = 1:length(m.Algo)
        KLDW(a+1) = mean(m.WC{a});
        KLDD(a+1) = mean(m.DC{a});
        KLDT(a+1) = mean([m.WC{a} m.DC{a}]);
    end

distance{1} = KLDW;
distance{2} = KLDD;
distance{3} = KLDT;

error=[]
return;
end

samples=40;
if (strcmp(type,'cdf'))
    bin=[2:2:20];
    idtl = length(bin)-1;
    type='kld';
else
    bin=10;
    idtl=bin-1;
end
id=[samples zeros(1,idtl)]';
e_id=id./sum(id);
% width
    n=hist([m.W{1}],bin)';
    e_n=n./sum(n);
    KLDW(1) = chist(id, n, type);    
    for a = 1:length(m.Algo)
        nn(:,a) =hist([m.WC{a}],bin);
        e_nn(:,a) = nn(:,a)./sum(nn(:,a));
        KLDW(a+1) = chist(id, nn(:,a), type);    
    end

% depth
    nd=hist([m.D{1}],bin)';
    e_nd = nd./sum(nd);
    KLDD(1) = chist(id, nd, type);
    for a = 1:length(m.Algo)
        nnd(:,a) =hist([m.DC{a}],bin);
        e_nnd(:,a) = nnd(:,a)./sum(nnd(:,a));
        KLDD(a+1) = chist(id, nnd(:,a), type);    
    end

% both width and depth
    nt=hist([m.W{1} m.D{1}],bin)';
    e_nt = nt./sum(nt);

    idt=[samples*2 zeros(1,idtl)]';
    e_idt = idt./sum(idt);

    KLDT(1) = chist(idt, nt, type);    
    for a = 1:length(m.Algo)
        nnt(:,a)=hist([m.WC{a} m.DC{a}],bin)   
        e_nnt(:,a) = nnt(:,a)./sum(nnt(:,a));
        KLDT(a+1) = chist(id, nnt(:,a), type);    
    end

error{1} = [e_id e_n e_nn];
error{2} = [e_id e_nd e_nnd];
error{3} = [e_idt e_nt e_nnt];

distance{1} = KLDW;
distance{2} = KLDD;
distance{3} = KLDT;
end

function dist = chist(h1, h2,type)

    switch(type)

        case 'chi2'
            dist = ChiSquare(h1,h2);    
        case 'kld'
            dist = KLD(h1,h2);
    end
end

function dist = KLD(h1,h2)
        h1=h1./sum(h1);
sum(h1)
        h2=h2./sum(h2);
sum(h2)
        dist = KLDiv(h1',h2');
end
function dist = ChiSquare(h1,h2)

    if(length(h1)~=length(h2))
        'Dimension not equal.'
    end
    Nr = (h1-h2).^2;
    Dr = h1+eps;
    frac = Nr./Dr
    dist = sum(frac);
    
end
