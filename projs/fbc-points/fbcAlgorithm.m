
% ipts - image points
% ipts3d - 3d points metric
% cp - camera instrinsics from checkboard method
% Np - number of set of points to use for calibraiton
% pmethod - method for camera pose estimation
    
function [cparams FBCerr CBerr] = fbcAlgorithm(imgnum, ipts, ipts3d, cp, pmethod)

    if length(ipts)~=length(ipts3d)
        'dimensions of inputs does not match'
        return
    end
    
%     % Np = number of images to be considered for fbc.
%     Np=1;
%     % extract set of points for calibration based on num of set of points
%     rng = randperm(length(ipts));
%     rng = rng(1:Np)
%     npts=[];
%     npts3d=[];
%     for obs = 1:length(rng)
%         % Gather three img feat points
%         rpts = [ipts{rng(obs)}(:,1:2);ipts{rng(obs)}(:,3:4)];
%         drpts = undistortPts(rpts',cp.d);
%         npts =[npts;[drpts' ones(6,1)]];
%         npts3d = [npts3d;[[ipts3d{rng(obs)}(:,1:2);ipts3d{rng(obs)}(:,3:4)] zeros(6,1)] ones(6,1)];
%     end

% extract set of imagepoints for imgnum
npts=[];
npts3d=[];
for in = 1: length(imgnum)
    rpts = [ipts{imgnum(in)}(:,1:2);ipts{imgnum(in)}(:,3:4)];
    drpts = undistortPts(rpts',cp.d, cp.k);
    npts =[npts; drpts'];
%     npts = [npts ones(6,1)]
% s=size(rpts)
% s=size(drpts)
%         npts =[npts; rpts];
    npts3d = [npts3d; [[ipts3d{imgnum(in)}(:,1:2);ipts3d{imgnum(in)}(:,3:4)] zeros(6,1)]];
%     npts3d = [npts3d ones(6,1)]
%     npts3d = [[ipts3d{imgnum}(:,1:2);ipts3d{imgnum}(:,3:4)] ones(6,1)];
%     npts3d = [npts3d(:,2) npts3d(:,3) npts3d(:,1)];
end
size(npts)
size(npts3d)
npts;
'loop';
in;
    % pose estimation based on pmethod
    switch(pmethod)
        case 'epfl'
            [R,T,Xc,best_solution]=efficient_pnp(npts3d,npts,cp.k); 
%             '--secondbtime----'
%             npts3d = npts3d(:,1:3);
%             npts = npts(:,1:2);
            [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
%             FBCerr
%             Urep
        
        case 'epflgauss'
            [R,T,Xc,best_solution]=efficient_pnp_gauss(npts3d,npts,cp.k);     
            [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
        
        case 'boug'
            fc=[cp.k(1,1);cp.k(1,1)];
            cc=[cp.k(1,3);cp.k(2,3)];
            [R,T]=compute_extrinsic(npts',npts3d',fc,cc);
             R= rodrigues(R);
            [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
       
        case 'gene'
            [R,T]=estimate_extrinsic_traditional(npts3d,npts,cp.k);     
            [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
            
        case 'zhang'
            [K, R, T] = CalibZhang(npts,npts3d(:,1:2),length(imgnum))
             R=Rodrigues(R)
            [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
            
%             [knew, dnew, R, T, rperr] = RefineCamParam2(npts, npts3d(:,1:2), cp.k, cp.d, Ri, Ti);
%             FBCerr=rperr;
%             npts_2 = undistortPts(npts',dnew, knew);
%             [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts_2',R,T,knew);
            
        otherwise
            'not implemented'
    end
    
%     refinment step
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d, cp.k, cp.d, R, T);
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam2(npts, npts3d(:,1:2), cp.k, cp.d, R, T);
%     [Knew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d, cp.k, R, T)

    % reprojection error
%     for p=1:length(ipts)
%         if imgnum==p
%             continue;
%         else
%             rpts = [ipts{p}(:,1:2);ipts{p}(:,3:4)];
%             drpts = undistortPts(rpts',cp.d, cp.k);
%             npts =[npts;[drpts ones(6,1)]];
%             npts3d = [npts3d;[[ipts3d{p}(:,1:2);ipts3d{p}(:,3:4)] zeros(6,1)]];
%     
%         end
%     end

%     [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T,cp.k);
%     FBCerr=FBCerr/size(npts3d,1);
%     err
%     err = err./(length(ipts)-1);
    
    %arrange output
%     cparams.k = Knew;
%     cparams.d = Dnew;
    cparams.k = cp.k;
    cparams.d = cp.d;
    cparams.Rt = R;
    cparams.Tt = T;
    cparams.Rm = R;
    cparams.Tm = T;
    cparams.Rl = R;
    cparams.Tl = T;

%     npts
%     npts3d
%     %estimate base error for checkerbaord
%     xchange_npts3d = [-1*npts3d(:,3) npts3d(:,1) zeros(length(npts3d),1)]
%     for j=1:length(xchange_npts3d)/6
%         ind = j*6-5:j*6;
%         [err1,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(1),:),npts(1,:),cp.Rt,cp.Tt,cp.k)
%         [err2,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(2),:),npts(2,:),cp.Rm,cp.Tm,cp.k)
%         [err3,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(3),:),npts(3,:),cp.Rl,cp.Tl,cp.k)
%         [err4,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(4),:),npts(4,:),cp.Rt,cp.Tt,cp.k)
%         [err5,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(5),:),npts(5,:),cp.Rm,cp.Tm,cp.k)
%         [err6,CBrep]=reprojection_error_usingRT(xchange_npts3d(ind(6),:),npts(6,:),cp.Rl,cp.Tl,cp.k)
%         err(j) = mean([err1 err2 err3 err4 err5 err6]);
%     end
%     CBerr=mean(err);

% FBCerr=rperr;
CBerr=[];

% % estimate error based on reprojection error
%     U=[];
%     Xw=[];
%     for ob = 1:length(rng)
%         Ud = undistortPts([ipts{rng(obs)}(:,1:2);ipts{rng(obs)}(:,3:4)]',cp.d);
%         U = [U;Ud'];        
%         Xw = [Xw;[ipts3d{rng(obs)}(:,1:2);ipts3d{rng(obs)}(:,3:4)] zeros(6,1)];
%     end
% %     U=npts(:,1:2)
% %     Xw = npts3d(:,1:3)
%     A=cparams.k;
%     [err,Urep]=reprojection_error_usingRT(Xw,U,R,T,A);
%     Urep;
%     err
end
