function [FBCcalib FBCerr FBCtime] = fbc_main(method, imgnum, fbcpts)

% run this only for generating one set of FBC calib paprameters
% that gives lowest possible RP

% creation of data - run the following
% ArrangeScannerData(1,'Gwest',f,g,h);
% ArrangeObsrevationData(o,meta);
% ArrangeReferenceMeas(raw);
% 
% load all scanner calibration data
load('dataset/CBcalib.mat');

% load features for calibration
% load ('fbc-points/fbcpts.mat')
% load ('fbc-points/fbcptsU.mat')
% fbcpts = fbcptsU;

% % all images are used for calibration
for scn = 1:length(fbcpts) % loop over the scanner
    for cam=1:length(fbcpts{scn}) % loop over the cameras in a scanner
        'for camera' ;
        cam    ;    
        ipts = fbcpts{scn}{cam}.FeatPts;
        ipts3d = fbcpts{scn}{cam}.RefPts;
        cp = CBcalib{scn}{cam};
        % gather data of all images in teh irange
        nipts=[];
        nipts3d=[];
        for in = 1: length(imgnum)
            nipts = [nipts; [ipts{imgnum(in)}(:,1:2);ipts{imgnum(in)}(:,3:4)]];
            nipts3d = [nipts3d; [[ipts3d{imgnum(in)}(:,1:2);ipts3d{imgnum(in)}(:,3:4)] zeros(6,1)]];
        end        
        [fbcparams FBCerr FBCtime] = PoseEstimate(nipts,nipts3d,cp.k, cp.d,method);
        cparams=[];
        cparams.k = fbcparams.k;
        cparams.d =  fbcparams.d;
        cparams.Rt = fbcparams.R;
        cparams.Tt = fbcparams.T;
        cparams.Rm = fbcparams.R;
        cparams.Tm = fbcparams.T;
        cparams.Rl = fbcparams.R;
        cparams.Tl = fbcparams.T;
        FBCcalib{scn}{cam}=cparams;
    end
end

end

