function FBCerr = CalibError(X,x,R,T,k)

tX=X;
ti=x;
tR=R;
tT=T;
tk=k;

% save tX tX
% save ti ti
% save tR tR
% save tT tT
% save tk tk

% Reprojection error
[rperr,Urep]=reprojection_error_usingRT(X,x,R,T,k);
FBCerr.rp = rperr;

% Back projection 
'....sizes';
size(x);
size(X);

P=k*[R,T];
X_new = P\[x ones(size(x,1),1)]';
div=repmat(X_new(4,:),4,1);
X_new = X_new./div;
x;
X_new = X_new(1:3,:)';
X;
% error- RMSE
% FBCerr.rmse = mse(X_new - X);
FBCerr.rmse = sqrt(sum((X_new - X).^2,2));

% error - NCCR

end