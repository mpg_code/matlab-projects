function [fbcparams FBCerr FBCtime] = PoseEstimate(ipts, ipts3d, k, d, pmethod)

    if length(ipts)~=length(ipts3d)
        'dimensions of inputs does not match'
        return
    end
% number of points in a image
NpImgs=6;
% undistort the image coordinates with known distortion coefficients.
npts = [undistortPts(ipts', d,  k)]';
npts3d = ipts3d;
size(npts)
size(npts3d)

% pose estimation based on pmethod
    switch(pmethod)
        case 'epfl'
            tic;
            [R,T,Xc,best_solution]=efficient_pnp(npts3d,npts, k); 
            et=toc;
        
        case 'epflgauss'
            tic;
            [R,T,Xc,best_solution]=efficient_pnp_gauss(npts3d,npts, k);     
            et=toc;
        
        case 'boug'
            fc=[ k(1,1); k(1,1)];
            cc=[ k(1,3); k(2,3)];
            tic;
            [R,T]=compute_extrinsic(npts',npts3d',fc,cc);
            et=toc;
            R= rodrigues(R);
       
        case 'gold'
            tic;
            [R,T]=estimate_extrinsic_traditional(npts3d,npts, k);
            et=toc;
            
        case 'zhang'
            tic;
            [K, R, T] = CalibZhang(npts,npts3d(:,1:2),size(npts,1)/NpImgs, k);
            et=toc;
            R=Rodrigues(R);
            
%         case 'zhang2'
%              tic;
%             [R,T]=estimate_extrinsic_zhangplanar(npts3d,npts, k);
%             et=toc;
            
        otherwise
            'not implemented'
    end

%     [FBCerr,Urep]=reprojection_error_usingRT(npts3d,npts,R,T, k);
    FBCerr = CalibError(npts3d, npts, R, T,  k);

    %     refinment step
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d,  k,  d, R, T);
%     [Knew, Dnew, Rnew, Tnew, rperr] = RefineCamParam2(npts, npts3d(:,1:2),  k,  d, R, T);
%     [Knew, Rnew, Tnew, rperr] = RefineCamParam(npts, npts3d,  k, R, T)
    fbcparams.k =  k;
    fbcparams.d =  d;
    fbcparams.R = R;
    fbcparams.T = T;
%     cparams.Rt = R;
%     cparams.Tt = T;
%     cparams.Rm = R;
%     cparams.Tm = T;
%     cparams.Rl = R;
%     cparams.Tl = T;
    
    FBCtime=et;
end
