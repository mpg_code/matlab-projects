function newfbcpts = undistortPantoFbcPoints(fbcpts, calib)
    newfbcpts=fbcpts;
    
    for i=1:length(fbcpts)
        for j=1:length(fbcpts{i})
            find=1;
            rind=1;
            for k=1:length(fbcpts{i}{j}.FeatPts) 
                
                cp=calib{i}{j};
                cpp=cameraParameters('IntrinsicMatrix',cp.k','RadialDistortion',cp.d(1:2), 'TangentialDistortion',cp.d(3:4));
                ipts = fbcpts{i}{j}.FeatPts{k};
                    
                 for pro = 1:3 
                    % three points are converted to corresp. 3d using corresp. profile calib data.
                    pts = reshape(ipts(pro,:),2,2)';
                    newpts = undistortPoints(pts, cpp);
                    newipts(pro,:) = reshape(newpts',1,4);
                 end

                    newfbcpts{i}{j}.FeatPts{k} = newipts;                
            end
        end
    end
