function ArrangeFBCcalibData(fp,rfp)

[r c ]= size(fp);
yoff = repmat([0 400], 3,2);
for i=1:r/3
    x=[i*3-2:i*3];
    y=[4:7];
    meta = fp(x(1),1:3);
    fpts{meta(1)}{meta(2)}.FeatPts{i} = fp(x,y) + yoff;
    fpts{meta(1)}{meta(2)}.RefPts{i} = rfp(meta(3)*3-2:meta(3)*3,:);
    
end

for i=1:length(fpts)
    for j=1:length(fpts{i})
        find=1;
        rind=1;
        for k=1:length(fpts{i}{j}.FeatPts)            
            if (~isempty(fpts{i}{j}.FeatPts{k}))
                fbcpts{i}{j}.FeatPts{find} = fpts{i}{j}.FeatPts{k};
                find=find+1;
            end
            if (~isempty(fpts{i}{j}.RefPts{k}))
                fbcpts{i}{j}.RefPts{rind} = fpts{i}{j}.RefPts{k};
                rind=rind+1;
            end
        end
    end
end
save fbcpts fbcpts;
load('dataset/CBcalib.mat');
fbcptsU = undistortPantoFbcPoints(fbcpts, CBcalib);
save fbcptsU fbcptsU;
end
