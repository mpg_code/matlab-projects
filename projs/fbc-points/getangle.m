function o= getangle(mat)

if size(mat,2)==3
    o = vision.internal.calibration.rodriguesMatrixToVector(mat)*180/pi;
end
if size(mat,2)==1
    o = vision.internal.calibration.rodriguesVectorToMatrix(mat/180*pi);
end
end
