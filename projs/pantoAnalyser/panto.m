function [WidthError DepthError Meas_est Meas_ref]= panto(type, calib, pantoref,obs)


    % Computation for every observation
   for oind = 1:length(obs)
%     for oind = 1:1
    %     extract metadatqa
        clear dmeta;
        dmeta = obs{oind}.meta;
        % Gather three image points
        ipts = obs{oind}.ImgPts;

        scanN=dmeta(1);
        camN = dmeta(2);
        pantoN=dmeta(3);
        defectN=dmeta(4);
        
        %     Estimate Measurement
        % From metadata find which calibration data to use
        cp=calib{scanN}{camN};
        Meas_est(oind) = pantoAnalysis(ipts,cp,type);

        %     Estimate Error
        % From metadata find ref measurements for specific panto and defect
        Meas_ref(oind) = pantoref{pantoN}{defectN};
        % compare the analysed measurement with reference measurement.
        WidthError(oind) = abs(Meas_est(oind).width - Meas_ref(oind).width);
        DepthError(oind) = abs(Meas_est(oind).depth - Meas_ref(oind).depth);

   end    
end

function PA_Meas_est = pantoAnalysis(ipts,cp, type)
    if (strncmp(type,'cb',10) && strncmp(type,'fbc',10))
        'ERROR: Unknown type of calib parameters'
        PA_Meas_est=NaN;
        return
    end
    
     dpts3D = BackProjectDefectPoints(ipts, cp);
%      dpts2D = ForwardProjectDefectPoints(dpts3D, cp);
%      vericheck = sum(ipts -dpts2D);
     
%     for every profile three points are obtained
      for pro = 1:length(dpts3D) 
          X1 = dpts3D{pro}(:,1);
          X2 = dpts3D{pro}(:,2);
          X3 = dpts3D{pro}(:,3);
          %estimate width and depth corres to each profile
           switch(type)
               case 'fbc'
                  %for fbc
                  int1=X3-X1;
                  int2=abs(X2-X1);
                  int3=abs(X2-X3);
                  width(pro) = int1(1);
                  depth(pro) = max(int2(3), int3(3));
               case 'cb'
                  int1=X3-X1;
                  int2=abs(X2-X1);
                  int3=abs(X2-X3);
                  width(pro) = int1(2);
                  depth(pro) = max(int2(1), int3(1));
               otherwise
                 width(pro) = NaN;
                 depth(pro) = NaN;
                 'WARNING: generated NaN';
           end
      end
      width;
      PA_Meas_est.width=max(width);
      depth;
      PA_Meas_est.depth=max(depth);
end

