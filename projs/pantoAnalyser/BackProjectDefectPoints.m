function dpts3D = BackProjectDefectPoints(dpts2d, cp)
      
      H{1}=cp.k*[cp.Rt,cp.Tt];
      H{2}=cp.k*[cp.Rm,cp.Tm];
      H{3}=cp.k*[cp.Rl,cp.Tl];
          
      
%     for every profile three points are obtained
      for pro = 1:3 
          % three points are converted to corresp. 3d using corresp. profile calib data.
          Npoints = length(dpts2d(pro,:))/2;
          pts = reshape(dpts2d(pro,:),2,Npoints);

          dpts3D{pro}=[];
          for p=1:Npoints
              x = pts(:,p);
              X = H{pro}\[x ;1];
              X = X./X(length(X));

              dpts3D{pro} = [dpts3D{pro} X(1:3)];
          end


%           x1 = pts(:,1);
%           x2 = pts(:,2);
%           x3 = pts(:,3);
% %           '2dpoints';
% %           [x1 x2 x3];
%          
%           
%           % back project it to 3D
%           X1 = H{pro}\[x1 ;1];
%           X2 = H{pro}\[x2 ;1];
%           X3 = H{pro}\[x3 ;1];
% 
%           X1 = X1./X1(length(X1));
%           X2 = X2./X2(length(X2));
%           X3 = X3./X3(length(X3));
% 
%           dpts3D{pro} = [X1(1:3) X2(1:3) X3(1:3)];
      end

%           '3dpoints';
%           [X1 X2 X3];
      
end