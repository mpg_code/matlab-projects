function xud = undistortPts(xd,dis, k)
    [r c] = size(xd);
    for i=1:c
        Xc=k\[xd(1,i);xd(2,i);1];
        Xc = Xc./Xc(3);
        pt.x = Xc(1);
        pt.y = Xc(2);
        RXc = getundistort(pt, dis);
        DXc = k * [RXc;1];
        DXc=DXc./DXc(3);
        xud(:,i) =DXc(1:2);
    end
   
end


function newx = getundistort(pt,dis)
    rsq = pt.x^2 + pt.y^2;
    dx1 = 2*dis(3)*pt.x*pt.y + dis(4)*(rsq + 2*pt.x^2);
    dx2 = dis(3)*(rsq + 2*pt.y^2) + 2*dis(4)*pt.x*pt.y;
    dx=[dx1;dx2];
    
    nx = 1 + dis(1)*rsq + dis(2)*(rsq^2) + dis(5)*(rsq^3);
    newx = [pt.x*nx;pt.y*nx] + dx;
end