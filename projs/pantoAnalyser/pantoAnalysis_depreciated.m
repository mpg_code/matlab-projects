function PA_Meas_est = pantoAnalysis_depreciated(ipts,cp, type)


    if (strncmp(type,'cb',10) && strncmp(type,'fbc',10))
        'ERROR: Unknown type of calib parameters'
        PA_Meas_est=NaN;
        return
    end
          
%     observ. has three profile observ.
    % obtain 3 homogrpahies
%       H{1}=computeHomo(cp.k,cp.Rt,cp.Tt);
%       H{2}=computeHomo(cp.k,cp.Rm,cp.Tm);
%       H{3}=computeHomo(cp.k,cp.Rl,cp.Tl);
      H{1}=cp.k*[cp.Rt,cp.Tt];
      H{2}=cp.k*[cp.Rm,cp.Tm];
      H{3}=cp.k*[cp.Rl,cp.Tl];
          
%     for every profile three points are obtained
      for pro = 1:3 
          % three points are converted to corresp. 3d using corresp. profile calib data.
          pts = reshape(ipts(pro,:),2,3);
          x1 = pts(:,1);
          xd1 = undistortPts(x1,cp.d,cp.k);
          x2 = pts(:,2);
          xd2 = undistortPts(x2,cp.d,cp.k);
          x3 = pts(:,3);
          xd3 = undistortPts(x3,cp.d,cp.k);
          
          % back project it to 3D
          X1 = H{pro}\[xd1 ;1];
          X2 = H{pro}\[xd2 ;1];
          X3 = H{pro}\[xd3 ;1];
'2dpoints';
          [xd1 xd2 xd3];
         
          
          X1 = X1./X1(length(X1));
          X2 = X2./X2(length(X2));
          X3 = X3./X3(length(X3));
 '3dpoints';
          [X1 X2 X3];
           %estimate width and depth corres to each profile
           switch(type)
               case 'fbc'
                  %for fbc
                  int1=X3-X1;
                  int2=abs(X2-X1);
                  int3=abs(X2-X3);
                  width(pro) = int1(1);
                  depth(pro) = max(int2(3), int3(3));
               case 'cb'
                  int1=X3-X1;
                  int2=abs(X2-X1);
                  int3=abs(X2-X3);
                  width(pro) = int1(2);
                  depth(pro) = max(int2(1), int3(1));
               otherwise
                 width(pro) = NaN;
                 depth(pro) = NaN;
                 'WARNING: generated NaN';
           end
      end
      
      width;
    PA_Meas_est.width=max(width);
      depth;
    PA_Meas_est.depth=max(depth);
end