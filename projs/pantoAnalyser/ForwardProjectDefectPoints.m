function dpts2d = ForwardProjectDefectPoints(dpts3D, cp)
 
      H{1}=cp.k*[cp.Rt,cp.Tt];
      H{2}=cp.k*[cp.Rm,cp.Tm];
      H{3}=cp.k*[cp.Rl,cp.Tl];
          
%     for every profile three points are obtained
      for pro = 1:3 
          % three points are converted to corresp. 3d using corresp. profile calib data.
        Npoints = length(dpts3D{pro}(1,:));
        pts=[];
        for p = 1: Npoints
              X = dpts3D{pro}(:,p);
              x = H{pro}*[X ;1];
              x = x./x(length(x));
              pts=[ pts x(1:2)];
        end
        dpts2d(pro,:) = reshape(pts,1,Npoints*2);

%           X1 = dpts3D{pro}(:,1);
%           X2 = dpts3D{pro}(:,2);
%           X3 = dpts3D{pro}(:,3);
%           
%           % forward project it to 2D
%           x1 = H{pro}*[X1 ;1];
%           x2 = H{pro}*[X2 ;1];
%           x3 = H{pro}*[X3 ;1];
% 
% %           '3dpoints';
% %           [X1 X2 X3];
%                 
%           x1 = x1./x1(length(x1));
%           x2 = x2./x2(length(x2));
%           x3 = x3./x3(length(x3));
% 
%           pts=[x1(1:2) x2(1:2) x3(1:2)];
%           dpts2d(pro,:) = reshape(pts,1,6);
%         
%           '2dpoints';
%           [x1 x2 x3];
       
      end

end
