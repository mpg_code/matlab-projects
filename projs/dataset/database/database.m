function [ output_args ] = database( feature, camera )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
centroid = [0 0 0]; % Center of sphere
R = 5: % radius of the sphere
N=1000;
rng(0,'twister');
rvals = 2*rand(N,1)-1;
elevation = asin(rvals);
azimuth = 2*pi*rand(N,1);
radii = R*(rand(N,1).^(10));
[x,y,z] = sph2cart(azimuth,elevation,radii);
figure
plot3(x,y,z,'.')
axis equal

xyz = [ x+centroid(1), y+centroid(2), z+centroid(3)]
end

