function ui()

%% obtain all parameters
    uiobj.points = 100; % object points
    uiobj.dist = 3000; % distance in mm
    uiobj.size = 1000; % radius in mm
    uiobj.mode = 'zeroZplane';
%     uiobj.mode = 'planar'; % planar / nonplanar
    
    % camera properties
    uicam.num = 1; % number of cameras

    % intrinsic
    uicam.fxy = [800 800]; % focal length x and y axis
    uicam.pxy = [300 200]; % principal x and y axis
    uicam.scale = 1; % scale factor
    uicam.distort = [0 0 0 0 0];
    
    %extrinsic
%     for single camera
     uicam.rot = [0;0;0];
     uicam.trans = [0;0;uiobj.dist];
%    only for more than 1 camera
%     uicam.topo = 'linear'; % topology of cameras
%     uicam.topo = 'circular';
%     uicam.baseline = 1000; % only for base distance R adm T will be calculated according to topology

    save uiobj uiobj
    save uicam uicam 
end