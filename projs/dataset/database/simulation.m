function output = simulation(varargin)

    %% read varargin
% define
%  arg1 - no of points
    if(length(varargin)>1)
        'wrong number of arguments'
        return
    else
        npoints = varargin{1}
    end


    %% Emulate ui
    ui();

    %% Extract necessary structures for computation
    [obj cam] = ExtractProps();
    obj;
    cam;
    
    %% Override if necessary
    obj.points = npoints;
    
    %% Position the object and get 3D positions
    X = PosObject(obj);
    
    %% Position the cameras and get camera parameters
    [K D R T] = PosCameras(cam, obj);
%     output = [K D R T]
    %% Projection from world to camera and to image place   
    [x Xc] = Proj3dto2dSingleCam(X,K,D,R,T);
    
    %% Back project 2d points to 3d  and measure accuracy in NCC and MSE
    [nccErr mseErr] = ErrorEst(X,x,K,D,R,T)
    
    
    %% save data
    data.X = X;
    data.K = K;
    data.D = D;
    data.R = R;
    data.T = T;
    data.Xc = Xc;
    data.x = x;
    save data data;
    
end

%%
function [obj cam] = ExtractProps()
    load('uiobj.mat');
    load('uicam.mat');
    
    obj = uiobj;
    cam = uicam;
    
% %     override
%        obj.mode = 'zeroXplane';
%        obj.mode = 'zeroYplane';
       obj.mode = 'zeroZplane';
    
%     for i=1:uicam.num
%         K{i} = [uicam.scale*uicam.fx 0 uicam.px;
%                 0 uicam.scale*uicam.fy uicam.py;
%                 0 0 1];
%         D{i} = uicam.distort;
%         R{i} = Rodrigues([uicam.rx; uicam.ry; uicam.rz]*pi/180);
%         T{i} = ([uicam.tx; uicam.ty; uicam.tz]);        
%     end
%     cam.K = K;
%     cam.D = D;
%     cam.R = R;
%     cam.T = T;
end


%%
function newX = PosObject(obj)

    switch(obj.mode)
        case 'zeroXplane'
            seg = obj.size;
            datx = zeros(1, obj.points);
            daty = seg*rand(1, obj.points);
            datz = seg*rand(1, obj.points);
            X =[datx;daty;datz]; % uniformly distributed points
            angx = 0;
            angy = -seg/2;
            angz = -seg/2;
            newX=X+repmat([angx; angy; angz],1,length(X));
            plot3(newX(1,:),newX(2,:),newX(3,:),'.');
        case 'zeroYplane'
            seg = obj.size;
            datx = seg*rand(1, obj.points);
            daty = zeros(1, obj.points);
            datz = seg*rand(1, obj.points);
            X =[datx;daty;datz]; % uniformly distributed points
            angx = -seg/2;
            angy = 0;
            angz = -seg/2;
            newX=X+repmat([angx; angy; angz],1, length(X));
            plot3(newX(1,:),newX(2,:),newX(3,:),'.');
        case 'zeroZplane'
            seg = obj.size;
            datx = seg*rand(1, obj.points);
            daty = seg*rand(1, obj.points);
            datz = zeros(1, obj.points);
            X =[datx;daty;datz]; % uniformly distributed points
            angx = -seg/2;
            angy = -seg/2;
            angz = 0;
            newX=X+repmat([angx; angy; angz],1, length(X));
            plot3(newX(1,:),newX(2,:),newX(3,:),'.');
        case 'planar' % uniform distributed 
            seg = obj.size;
            X =[seg*rand(2, obj.points); zeros(1, obj.points)]; % uniformly distributed points
            newX=X+repmat([-seg/2; -seg/2; 0],1, length(X));
%             plot3(newX(:,1),newX(:,2),newX(:,3),'.');
            Rdeg = (180*rand(3,1) + [-90 -90 -90]')
            R = Rodrigues(Rdeg.*pi./180);
            newX = (newX'*R)';
            plot3(newX(1,:),newX(2,:),newX(3,:),'.');
            
        case 'sphere'
            
    end
end

%%
function [K D R T] = PosCameras(cam, obj)

% % reference camera
%     r12 = [0 15 5];%*pi/180; % rotation x,y,z axis in radians
%     t12 = [100 50 0]; % translation x,y,z axis in mm
%     
    %preprocess before storing for homogenoues camera array
    for i=1:cam.num
        K = [cam.scale*cam.fxy(1) 0 cam.pxy(1);
                0 cam.scale*cam.fxy(2) cam.pxy(1);
                0 0 1];
        D = cam.distort;
        if (i==1)
            R = Rodrigues(cam.rot);
            T = cam.trans;
        else
            'not supported for cameras more than one'
            switch(cam.topo)
                case 'linear'
                    newR = Rodrigues([0 0 0]*pi/180);
                    newT = [cam.baseline 0 0];
                    break;
                case 'circular'
                    'not supported'
                    break;
            end
            R{i} = R{i-1}.newR;
            T{i} = T{i-1} + newT;
        end
    end
 end


%%
function [x Xc] = Proj3dto2dMultiCam(X,K,D,R,T)

    [r c] = size(X);
    %test condition for size
    assert(length(R)==length(T)==length(D)==length(K))

    for i=1:length(R)
       [x{i} Xc{i}] = Proj3dto2dSingleCam(X,K,D,R,T);
    end
end

%%
function [x Xc] = Proj3dto2dSingleCam(X,K,D,R,T)

    [r c] = size(X);

    % world to camera coordinates
    Xc = [[R T]*[X ; ones(1,c)]];
    % camea to pixel coordinates
    xi = K * Xc;
    l = xi./repmat(xi(3,:),3,1);
    x = l(1:2,:);

end


%%
function [nccErr mseErr] = ErrorEst(X,x,K,D,R,T)

    [r3 c3] = size(X);

%     %test condition for size
%     assert(length(R)==length(T)==length(D)==length(K))
%     for j = 1: length(R)
%        Xc = K{j}\[x{j}; ones(1,c3)];
%        int = [R{j} T{j}] \ Xc;
%        int = Xest./repmat(Xest(4,:),4,1)
%        Xest = int(1:3,:);
%     end

       Xc = K\[x; ones(1,c3)];
       int = [R T] \ Xc;
       int = int./repmat(int(4,:),4,1);
       Xest = int(1:3,:);

       mseErr = mse(X-Xest);
       nccErr = 0;
end
