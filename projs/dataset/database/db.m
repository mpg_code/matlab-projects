classdef db
    %DB Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    % object properties
    objdist = 3000; % distance in mm
    objsize = 1000; % radius in mm
    objmode = 'planar'; % planar / nonplanar
    % camera properties
    camnum = 1; % number of cameras
    camtopo = 'none'; % topology of cameras
    % intrinsic
    fx = 800; % focal length x axis
    fy = 800; % focal length y axis
    px = 300; % principal x axis
    py = 200; % principal y axis
    scale = 1; % scale factor
    %extrinsic
    rx = 0; % rotation x axis
    ry = 0; % rotation y axis
    rz = 0; % rotation z axis
    tx = 0; % translation x axis
    ty = 0; % translation y axis
    tz = 0; % translation z axis
    
    end
    
    methods
        function out = simulate()
            
        end
            
    end
    
end

