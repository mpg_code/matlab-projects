function obsU = ArrangeObsrevationData(o)

[r c ]= size(o);
yoff = repmat([0 400], 3,3);
for i=1:r/3
    x=[i*3-2:i*3];
    y=[5:10];
    obs{i}.ImgPts =o(x,y) + yoff;
    obs{i}.PantoMeas.width =o(x,11);
    obs{i}.PantoMeas.depth =o(x,12);
    obs{i}.meta=o(x(1),1:4);
    save obs obs;
end
% obsU=obs;
load('dataset/CBcalib.mat');
obsU = undistortPantoObsPoints(obs, CBcalib);
save obsU obsU;