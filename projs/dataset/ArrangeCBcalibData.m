
function CBcalib = ArrangeCBcalibData(f)

% Once f - for 3 cameras are obtained from excel sheet for a particular scanner

scannum = f(16,1);
% Arrange scanner{scannum}{1} components 
CBcalib{scannum}{1}.k=reshape(f(1,1:9),3,3)';
CBcalib{scannum}{1}.d=reshape(f(2,1:5),5,1)';
CBcalib{scannum}{1}.Rt=reshape(f(3,1:9),3,3)';
CBcalib{scannum}{1}.Tt=reshape(f(3,10:12),3,1);
CBcalib{scannum}{1}.Rm=reshape(f(4,1:9),3,3)';
CBcalib{scannum}{1}.Tm=reshape(f(4,10:12),3,1);
CBcalib{scannum}{1}.Rl=reshape(f(5,1:9),3,3)';
CBcalib{scannum}{1}.Tl=reshape(f(5,10:12),3,1);

% Arrange scanner{scannum}{2} components
CBcalib{scannum}{2}.k=reshape(f(6,1:9),3,3)';
CBcalib{scannum}{2}.d=reshape(f(7,1:5),5,1)';
CBcalib{scannum}{2}.Rt=reshape(f(8,1:9),3,3)';
CBcalib{scannum}{2}.Tt=reshape(f(8,10:12),3,1);
CBcalib{scannum}{2}.Rm=reshape(f(9,1:9),3,3)';
CBcalib{scannum}{2}.Tm=reshape(f(9,10:12),3,1);
CBcalib{scannum}{2}.Rl=reshape(f(10,1:9),3,3)';
CBcalib{scannum}{2}.Tl=reshape(f(10,10:12),3,1);

% Arrange scanner{scannum}{3} components
CBcalib{scannum}{3}.k=reshape(f(11,1:9),3,3)';
CBcalib{scannum}{3}.d=reshape(f(12,1:5),5,1)';
CBcalib{scannum}{3}.Rt=reshape(f(13,1:9),3,3)';
CBcalib{scannum}{3}.Tt=reshape(f(13,10:12),3,1);
CBcalib{scannum}{3}.Rm=reshape(f(14,1:9),3,3)';
CBcalib{scannum}{3}.Tm=reshape(f(14,10:12),3,1);
CBcalib{scannum}{3}.Rl=reshape(f(15,1:9),3,3)';
CBcalib{scannum}{3}.Tl=reshape(f(15,10:12),3,1);


save CBcalib CBcalib;
end


