function pantoref = ArrangeReferenceMeas(raw)

[r c ]= size(raw);
for i=1:c/2
    y=[i*2-1:i*2];
    for j=1:4
        RefMeas{j}.width = raw(j,y(1));
        RefMeas{j}.depth = raw(j,y(2));
    end
    pantoref{i}=RefMeas;
end

save pantoref pantoref
end
