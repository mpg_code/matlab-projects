function newobs = undistortPantoObsPoints(obs, calib)
    newobs=obs;
    for oind = 1:length(obs)
        dmeta = obs{oind}.meta;
        % Gather three image points
        ipts = obs{oind}.ImgPts;

        scanN = dmeta(1);
        camN = dmeta(2);
        % From metadata find which calibration data to use
        cp=calib{scanN}{camN};
        
        cpp=cameraParameters('IntrinsicMatrix',cp.k','RadialDistortion',cp.d(1:2), 'TangentialDistortion',cp.d(3:4));
        
         for pro = 1:3 
            % three points are converted to corresp. 3d using corresp. profile calib data.
            pts = reshape(ipts(pro,:),2,3)';
            newpts = undistortPoints(pts, cpp);
            newipts(pro,:) = reshape(newpts',1,6);
         end
         newobs{oind}.ImgPts = newipts;
    end
end