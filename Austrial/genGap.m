function dev =genGap(g, H, off)

x1 = 850;
x2 = 895;


    for st= 1 : 400-g-1 

        r = randperm(x2-x1);
        rng = r(1);

        X1=[st+g x1 0 1]';
        X2=[st x1+rng 0 1]';
        X3=[st+g x2 0 1]';


        X=[X1 X2 X3];

        x = get2Dpts(X, H);
        nxdis(1,:)=x(2,:) - ones(1,3).*off;
          nxdis(2,:)=x(1,:)- ones(1,3).*off;
          h2(st) = getPdist(nxdis);
          xaxe(st)= st+off;
    end

    dev =[mean(h2) std(h2)];
%     dev = max(abs(h2-ones(size(h2,1),size(h2,2)) * g));
    figure, plot(xaxe,h2)
end

function x = get2Dpts(X, H)

ind = size(X,2);

for c=1:ind
    int=H * X(:,c);
    int1 = int./int(3);
    x(:,c) = int1(1:2);
end


end