function [h3 h2 ] = pantoThick(ipts,cam, yoff)

%     take care of ofset
      ipts(2,:) =  ipts(2,:) + ones(1,6).*yoff;

     load('Austrial/CBcalib.mat');
        cp = CBcalib{1}{cam};
        cp.Rm = getangle(getangle(cp.Rm)+[0;0;0]);
        cp.Tm = cp.Tm + [1000;0;0];
     
%     observ. has three profile observ.
    % obtain 3 homogrpahies
      H{1}=computeHomo(cp.k,cp.Rt,cp.Tt);
      H{2} = computeHomo(cp.k,cp.Rm,cp.Tm);
      H{3}=computeHomo(cp.k,cp.Rl,cp.Tl);
          
%     for every profile three points are obtained
      for pro = 2%1:3 
          % three points are converted to corresp. 3d using corresp. profile calib data.
          pts = reshape(ipts(pro,:),2,3);
          x1 = pts(:,1);
          xd1 = undistortPts(x1,cp.d,cp.k);
          x2 = pts(:,2);
          xd2 = undistortPts(x2,cp.d,cp.k);
          x3 = pts(:,3);
          xd3 = undistortPts(x3,cp.d,cp.k);
          
          'distorted 2d'
          xdis=[x1 x2 x3];
          nxdis(1,:)=xdis(2,:);
          nxdis(2,:)=xdis(1,:);
          h2 = getPdist(nxdis);
          
          % back project it to 3D
          X1 = H{pro}\[xd1' ;1];
          X2 = H{pro}\[xd2' ;1];
          X3 = H{pro}\[xd3' ;1];
        '2dpoints';
          [xd1' xd2' xd3'];
   
          
          X1 = X1./X1(length(X1));
          X2 = X2./X2(length(X2));
          X3 = X3./X3(length(X3));

           h3 = getPdist([X1 X2 X3]);
%           a = X1(1:2,:);
%           b = X2(1:2,:);
%           c = X3(1:2,:);
%           
%           '3dpoints';
%           [X1 X2 X3]
% 
%           h = det([a-c c-b])/norm(a-c);
%           
%           int1 = abs(X2-X1);
%           int2 = abs(X2-X3);
%           h = min(int1(1),int2(1));
% %           h(pro) = perDist(X1, X2, X3);
          
    
      end
    end

function h = perDist(A,B,C)
    a = distform (B,C)
    b = distform (A,C)
    c = distform (A,B)
    
    ang = acos((b^2 + a^2 - c^2)/2*b*a)
    h = sin(ang)*a;
end

function dist = distform(a,b)
        dist = sqrt((a(2)-b(2)).^2 + (a(1)-b(1)).^2);
end


% for i=length(yoff)
% clear ipts;
% load ipts1;
% ipts(2,:) =  ipts(2,:) + ones(1,6).*yoff(i)
% h(i)=pantoThick(ipts, 2);
% end
