function h = getPdist(X)

  X1 = X(:,1);
  X2 = X(:,2);
  X3 = X(:,3);

  a = X1(1:2,:);
  b = X2(1:2,:);
  c = X3(1:2,:);

  '3dpoints';
  [X1 X2 X3];

  h = det([a-c c-b])/norm(a-c);

end