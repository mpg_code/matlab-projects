function testrun(varparam, varaxis)

%% test condition
relR = [0 0 0];
relT = [10 0 0];

%varparam = 'rot';
%varaxis = 'xaxis';

%% Initialization
% X3d = [0,0,100; 10,0,100; 0,10,100; 0,0,90; 0,0,110; -10,0,100; 0,-10,100];
% cubesize = 5; % length of cube both on neg and pos side including the origin
% X3d=[];
% scale=10;
% for x = 1:cubesize
%     for y = 1:cubesize
%         for z = 1:cubesize
%             X3d = [X3d;[x,y,z]*scale - ((cubesize +1)* scale/2)];
%         end
%     end
% end
X3d=randn(1000,3)*10;
%figure,plot3(X3d(:,1),X3d(:,2),X3d(:,3),'*')
%plot3(X3d(:,1),X3d(:,2),X3d(:,3),'*')
%% intrinsics
focal = 0.055; % in mts
res = [1920 1080]; % hd
skew = 0;

K = [focal skew res(1)/2; 0 focal res(2)/2; 0 0 1];

%% extrinsics
R1 = Rodrigues([0 0 0]);
T1 = [0 0 0]';
R2 = Rodrigues(relR.*pi./180);
T2 = relT';

%% Projection matrix
P1 = [K * R1 K*T1];
P2 = [K * R2 K*T2];

%% image points
X3d_H = [X3d ones(size(X3d,1),1)]';
x2d1 = P1 * X3d_H;
x2d2 = P2 * X3d_H;
for i=1:length(x2d1)
    x2d1(:,i) = x2d1(:,i)./x2d1(3,i);
    x2d2(:,i) = x2d2(:,i)./x2d2(3,i);
end
x2d1(3,:)=[];
x2d2(3,:)=[];


%%new projection for backprojection - emulation change in extrinsics
int = [-10:1:10]';
if(varaxis=='xaxis')arr = [int zeros(size(int,1),1) zeros(size(int,1),1)];end;
if(varaxis=='yaxis')arr = [zeros(size(int,1),1) int zeros(size(int,1),1)];end;
if(varaxis=='zaxis')arr = [zeros(size(int,1),1) zeros(size(int,1),1) int];end;

Rnew = R2;
Tnew = T2;

figure1 = figure;

for i=1:size(arr,1)
    if(strcmp(varparam,'rot') )
        Rnew = Rodrigues(arr(i,:)*pi/180);
%         xlabel('Rotation Variation','FontSize',14);
    end
    if(strcmp(varparam,'tran') )
        Tnew = T2+arr(i,:)'; 
%         xlabel('Translation Variation','FontSize',14);

    end
    P2new = [K*Rnew K*Tnew];
    Error(i,:) = GetTriError(X3d,x2d1,x2d2,P1,P2new)
end
% ylabel('3D Error','FontSize',14); 


% Create axes
axes1 = axes('Parent',figure1);
%,'XTickLabel',{'-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10'},...
 %   'XTick',int
% plot1 = plot(Error,'Parent', axes1);
plot1 = plot(int,Error);
set(plot1(1),'DisplayName','X 3d');
set(plot1(2),'DisplayName','Y 3d');
set(plot1(3),'DisplayName','Z 3d');

legend1 = legend(axes1,'show');
% rarr=[];
% tarr=[];
% axis=0;
% switch(varaxis)
%     case 'xaxis'
%         axis=1;
%     case 'yaxis'
%         axis=2
%     case 'zaxis'
%         axis=3
% end
% switch(varparam)
%     case 'rot'
%        %rarr = [relR(axis)*0.1:relR(axis)*0.1:relR(axis)*2]'; 
%        int = [-10:1:10]';
%        if(varaxis=='xaxis')rarr = [int zeros(size(int,1),1) zeros(size(int,1),1)];end;
%        if(varaxis=='yaxis')rarr = [zeros(size(int,1),1) int zeros(size(int,1),1)];end;
%        if(varaxis=='zaxis')rarr = [zeros(size(int,1),1) zeros(size(int,1),1) int];end;
% 
%        for i=1:size(rarr,1)
%             Rnew = Rodrigues(rarr(i,:)*pi/180); 
%             Tnew = T2;
%             P2new = [K*Rnew K*Tnew];
%             Error(i,:) = GetTriError(X3d,x2d1,x2d2,P1,P2new);
%             
%        end
%        figure, plot(Error);
%        
%     case 'tran'
%        tarr = [relT(axis)*0.1:relT(axis)*0.1:relT(axis)*2]';
%        tarr = [tarr zeros(size(tarr,1),1) zeros(size(tarr,1),1)];
%        for i=1:size(rarr,1)
%             Rnew = R2; 
%             Tnew = tarr(i,:);
%             P2new = [K*Rnew K*Tnew];
%             Error(i,:) = GetTriError(X3d,x2d1,x2d2,P1,P2new);
%             
%        end
%        figure, plot(Error);
% end
end

function error = GetTriError(X3d,x2d1,x2d2,P1,P2new)
     %% triangulation
    Est_X3d = triangulate(x2d1',x2d2',P1',P2new');

    %% 3d error
    error =sum(abs(X3d - Est_X3d))/size(X3d,1);
end
