Vi bruger kun felterne indtil blyantstregen �verst og nederst i h�jre side af kalibreringspladen.

St�rrelse af felt: 30 x 30 mm

Antal felter: 16 x 38


--------------------
Intrinsic resultater:

Focal Length:          fc = [ 4108.3639532   4116.3119982 ] � [ 3.23901   1.97529 ]
Principal point:       cc = [ 952.4509924   533.6819005 ] � [ 2.34433   4.83615 ]
Skew:             alpha_c = [ 0.0000000 ] � [ 0.00000  ]   => angle of pixel axes = 90.00000 � 0.00000 degrees
Distortion:            kc = [ -0.1631802   0.5562001   0.0018775   -0.0033889  0.0000000 ] � [ 0.00180   0.02800   0.00016   0.00012  0.00000 ]
Pixel error:          err = [ 0.0846566   0.2303960 ]

--------------------
Extrinsic - top:

Translation vector: Tc_ext = [ -551.9876105 	 -138.1778946 	 2816.3720181 ]
Rotation vector:   omc_ext = [ -1.8147617 	 -1.7954346 	 -0.6317996 ]
Rotation matrix:    Rc_ext = [ 0.0194769 	 0.9995223 	 -0.0239968
                               0.7642313 	 0.0005919 	 0.6449420
                               0.6446481 	 -0.0309005 	 -0.7638547 ]
Pixel error:           err = [ 0.06278 	 0.19339 ]

--------------------
Extrinsic - mid:

Translation vector: Tc_ext = [ -552.6278964 	 -129.2025459 	 2805.8719046 ]
Rotation vector:   omc_ext = [ -1.8147610 	 -1.7954008 	 -0.6318732 ]
Rotation matrix:    Rc_ext = [ 0.0194817 	 0.9995231 	 -0.0239578
                               0.7642018 	 0.0005645 	 0.6449770
                               0.6446830 	 -0.0308738 	 -0.7638263 ]
Pixel error:           err = [ 0.06203 	 0.19034 ]

--------------------
Extrinsic - low:

Translation vector: Tc_ext = [ -553.0766447 	 -122.9345575 	 2798.5668792 ]
Rotation vector:   omc_ext = [ -1.8147719 	 -1.7953687 	 -0.6318811 ]
Rotation matrix:    Rc_ext = [ 0.0195041 	 0.9995228 	 -0.0239531
                               0.7641921 	 0.0005449 	 0.6449884
                               0.6446937 	 -0.0308847 	 -0.7638169 ]
Pixel error:           err = [ 0.06090 	 0.19206 ]

