
function out = runrecalib()
% function out = runrecalib(datapath,kname, rname, tname)

% generate 3d points 
tn=1000;%200 train passages and around 5 measurements per passage.
    P=synPantoObs(tn);
    sz=size(P);
%     for i=1:sz(2)
    for i=1:1
X1 = P(:,i,1);
X2 = P(:,i,2);

% generate corresponding 2d points with known calib params
% assuming we start with 2d points

% intrinsic = fullfile(datapath,kname)
% rotation = fullfile(datapath,rname)
% translation = fullfile(datapath,tname)
% load(intrinsic);
% load(rotation);
% load(translation);

load K;
load R1;
load T1;

Hf = computeHomo(k, r, t);
X1(3) = 1;
x1 = Hf * X1;

X2(3) = 1;
x2 = Hf * X2;

% camera motion emulation
rot=[0;20*pi/180;0];
trans=[0;0;0];
x1_new=emulcam(x1, k, rot, trans);
x2_new=emulcam(x2, k, rot, trans);

% back projection with known KRT
error = backproject(X1, X2, x1_new, x2_new, k, r, t)

% estimate camera motion
est_rot=[20*pi/180;0;0];
est_trans=trans;

<<<<<<< HEAD
<<<<<<< HEAD
% adjust RT - obtain NEWKRT
r_new = r'*est_rot;
t_new = est_trans+t;
=======
% misalignemnt correction
x1_corr=emulcam(x1_new, k, -est_rot, -est_trans);
x2_corr=emulcam(x2_new, k, -est_rot, -est_trans);
>>>>>>> 34d103aad4cf733bc0ebfff3d05a3cd86d19ca89
=======
% misalignemnt correction
x1_corr=emulcam(x1_new, k, -est_rot, -est_trans);
x2_corr=emulcam(x2_new, k, -est_rot, -est_trans);
=======
% adjust RT - obtain NEWKRT
r_new = r'*est_rot;
t_new = est_trans+t;
>>>>>>> 4cd5c0de2d08db1a1814efc3bd26e45d83ea2160
>>>>>>> 11fae2f9da4c179eb98741b960f21c065415097a

% back projection with known NEWKRT
error_new = backproject(X1, X2, x1_corr, x2_corr, k, r, t)

% % estimate camera motion
% est_rot=vision.internal.calibration.rodriguesVectorToMatrix([rot]);
% est_trans=[0;0;0];
% % adjust RT - obtain NEWKRT
% r_new = est_rot*r;
% t_new = est_trans+t;
% 
% % back projection with known NEWKRT
% error_new = backproject(X1, X2, x1_new, x2_new, k,r_new,t_new)

% evaluate 3d measurement errors due to known and recalib KRTs
% error
% error_new
% diff(i)=(error-error_new)/error*100;
diff(i)=error-error_new;
    end
    
%     out =diff;
end

function error = backproject(X1, X2, x1_new, x2_new, k, r, t)
H = computeHomo(k, r, t);
X1_new = H\x1_new;
X1_new = X1_new/X1_new(3);
X2_new = H\x2_new;
X2_new = X2_new/X2_new(3);
% measure error
W = abs(X2(2) - X1(2));
W_est = abs(X2_new(2)-X1_new(2));
error=abs(W - W_est);

end
function x_new=emulcam(x, k, rot, trans)
% rot
% trans
cc = k\x;
rrot = vision.internal.calibration.rodriguesVectorToMatrix(rot);
%rrot = rodrigues(rot);
cc(4)=1;
ccn = [rrot trans; [0 0 0 0]] * cc;
x_new = [k [0;0;0]] * ccn;


end

function Hf = computeHomoFor(k, r, t)
        tf=-1.*(r*t);
        rtf=[r(:,1:2) tf];
        Hf=k*rtf;
end

function H = computeHomo(k, r, t)
        rt=[r(:,1:2) t];
        H=k*rt;
end