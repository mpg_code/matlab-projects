% % function []=evalSingleCalib()
% % output: error in the measurement
% % input: X - 3Dcalibration params: type, K, R, T
% % % % 2dtype - 'points', 'cracks', 'depths'

function [X_new]=evalSingleCalib(X, k, r, t, rot, trans)

%%% initial check
% if length(find(size(t)==3))!=1 %if rrot is a vector 
    
    
% % project to 2d image points based on calib parameters
Hf = computeHomo(k, r, t);
X(3) = 1;
x = Hf * X; %X is 3x1 with z=0 and H is 3x3

%alternative tform matlab approach
% T = [r(1, :); r(2, :); t'] * k;
% tform = projective2d(T);
% [u,v] = transformPointsForward(tform, X(1),X(2));
% x=[u;v;1]

% emulate camera misalignement and estimate 2d projective transform
cc = k\x;
rrot = vision.internal.calibration.rodriguesVectorToMatrix(rot);
%rrot = rodrigues(rot);
cc(4)=1;
ccn = [rrot trans; [0 0 0 0]] * cc;
x_new = [k [0;0;0]] * ccn;




% % back project 2d image points to 3d measurements
H = computeHomo(k, r, t);
X_new = H\x_new;
X_new = X_new/X_new(3);
% [g,h] = transformPointsInverse(tform, x_new(1),x_new(2));
% X_new=[g h 1];

return
    


% % % % % % % % % % % % % % % % % % % % % 
% local functions
% % % % % % % % % % % % % % % % % % % % % 

% % function H = computeHomo(k, r, t)
% % output: intrinsic, rotation and translation matrix
% % input: homogrpahy
function H = computeHomo(k, r, t)
        rt=[r(:,1:2) t];
        H=k*rt;
return
function Hf = computeHomoFor(k, r, t)
        tf=-1.*(r*t);
        rtf=[r(:,1:2) tf];
        Hf=k*rtf;
return

% % function [X,Y,W,H] = get3dMeasurements(h,x,y,zx,zy)
% % output: X,Y = 3d points, W = 3d width, H = 3d height
% % input: h=homography, x,y,zx,zy = 2d points
function [X,Y,W,H] = get3dMeasurements(h,x,y,zx,zy)
    X=[];Y=[];W=[];H=[];
    %get 3d measurements
    X = compute3dPts('planar', x, h);
    Y = compute3dPts('planar', y, h);
    W=abs(X-Y);
    W=W(1,:);
    if (~isempty(zx) && ~isempty(zy))
        ZX = compute3dPts('planar', zx, h);
        ZY = compute3dPts('planar', zy, h);
        H = max(ZX(2)-X(2),ZY(2)-Y(2));
    end
return


