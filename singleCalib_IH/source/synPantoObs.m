function [out ] = synPantoObs(n)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if n=='test'
    testSynPanto();
    return;
end
xlim = 500; %mm
ylim = 1500; %mm
width = 50; %mm
depth = 30; %mm

a=[rand(n,1)'+xlim; rand(n,1)'*ylim; zeros(n,1)'];
b=[rand(n,1)'+xlim; a(2,:)+rand(n,1)'*width; zeros(n,1)'];
gap=b(2,:)-a(2,:);

c=[a(1,:)+rand(n,1)'*depth; a(2,:)+rand(n,1)'.*gap; zeros(n,1)'];

% c=[a(1,:)+rand(n,1)'*depth; a(2,:); zeros(n,1)'];
% d=[b(1,:)+rand(n,1)'*depth; b(2,:); zeros(n,1)'];

% a=[rand(n,1)+500 rand(n,1)*1000]';
% b=[rand(n,1)+500 a(2,:)'+rand(n,1)*10]';
% c=[a(1,:)'+rand(n,1)*20 a(2,:)'+rand(n,1)*3]';
% d=[b(1,:)'+rand(n,1)*20 b(2,:)'-rand(n,1)*3]';

out(:,:,1)=a;
out(:,:,2)=b;
out(:,:,3)=c;
% out(:,:,4)=d;
end


function testSynPanto()
p=synPantoObs(10);
sz=size(p);
for i=1:sz(2)
    figure(10),plot(p(2,i,1),p(1,i,1),'b*')
    hold on
    plot(p(2,i,2),p(1,i,2),'r*')
    plot(p(2,i,3),p(1,i,3),'g*')
%     plot(p(2,i,4),p(1,i,4),'y*')
%     plot([p(:,i,1) p(:,i,2) p(:,i,3) p(:,i,4)]);
    hold off
    pause
end
end
