function [ err ] = runeval(datapath,kname, rname, tname)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%file strings
intrinsic = fullfile(datapath,kname);
rotation = fullfile(datapath,rname);
translation = fullfile(datapath,tname);
resultsfolder = fullfile(datapath,'results');
mkdir(resultsfolder)
taxesfstr = fullfile(resultsfolder,'taxes');
raxesfstr = fullfile(resultsfolder,'raxes');
tmeasure = fullfile(resultsfolder,'tdiff');
rmeasure  = fullfile(resultsfolder,'rdiff');

%eval params
tmin = -100; tmax = 100; 
rmin = -40; rmax = 40;
taxes = [tmin:tmax];
raxes = [rmin:rmax];
save (taxesfstr, 'taxes');
save (raxesfstr, 'raxes');

% load calib params
load(intrinsic);
load(rotation);
load(translation);
    
%Pure translation measurement
%movement for 1cm on either sides for 3 translations
rot = [0;0;0];
for tdir = 1:3
    trans = [0;0;0];
    for d =tmin:tmax
        trans(tdir) = d; 
        [tPerr, werr, derr] = MeasErr(k,r,t,rot,trans);
        tdiff(:,d+tmin*-1+1,tdir)=[tPerr werr derr]';
    end
end
save(tmeasure,'tdiff');

%Pure rotation measurement
% %movement for 1degree/radian on either sides for 3 rotations
trans = [0;0;0];
for rdir = 1:3
    rot = [0;0;0];
    for s =rmin:rmax
        rot(rdir) = s;
        rot = rot.*pi./180;
        [tPerr, werr, derr] = MeasErr(k,r,t,rot,trans);
        rdiff(:,s+rmin*-1+1,rdir)=[tPerr werr derr]';
    end
end
save(rmeasure,'rdiff');


% plotres();
end

