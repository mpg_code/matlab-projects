function [tPerr, werr, derr] = MeasErr(k,r,t,rot,trans)

    % measurements synthesize in world system
    tn=1000;%200 train passages and around 5 measurements per passage.
    P=synPantoObs(tn);
    sz=size(P);
    for i=1:sz(2)
%         X4 = evalSingleCalib(P(:,i,4), k, r, t, rot, trans);
%         X4 = emulateCam(P(:,i,4), k, r, t, rot, trans);
%         X4=X4./X4(3);
%         X4=X4(1:2);
        
        X3 = evalSingleCalib(P(:,i,3), k, r, t, rot, trans);
%         X3 = emulateCam(P(:,i,3), k, r, t, rot, trans);
%         X3=X3/X3(3);
%         X3=X3(1:2);
        
        X2 = evalSingleCalib(P(:,i,2), k, r, t, rot, trans);
%         X2 = emulateCam(P(:,i,2), k, r, t, rot, trans);
%         X2=X2/X2(3);
%         X2=X2(1:2);
        
        X1 = evalSingleCalib(P(:,i,1), k, r, t, rot, trans);
%         X1 = emulateCam(P(:,i,1), k, r, t, rot, trans);
%         X1=X1/X1(3);
%         X1=X1(1:2);
        
        %points
        threeP = [P(1:2,i,1) P(1:2,i,2) P(1:2,i,3)];% P(:,i,4)];
        threeP_est = [X1(1:2) X2(1:2) X3(1:2)];% X4];
        %width
        W = abs(P(2,i,2) - P(2,i,1));
        W_est = abs(X2(2)-X1(2));
        %depth
        Dcrit = P(1,i,1) - P(1,i,2);
        if Dcrit>=0
            D = abs(P(1,i,3) - P(1,i,1));
            D_est = abs(X3(1)-X1(1));
        else
            D = abs(P(1,i,3) - P(1,i,2));
            D_est = abs(X3(1)-X2(1));
        end
%         Dcrit = P(1,i,3) - P(1,i,4);
%         if Dcrit>=0
%             D = abs(P(1,i,3) - P(1,i,1));
%             D_est = abs(X3(1)-X1(1));
%         else
%             D = abs(P(1,i,4) - P(1,i,2));
%             D_est = abs(X4(1)-X2(1));
%         end
        tPerror(i) = norm(mean((threeP-threeP_est),2));
        werror(i) = norm(W-W_est);
        derror(i) = norm(D-D_est);
    end
    tPerr=mean(tPerror);
    werr=mean(werror);
    derr=mean(derror);
end