
function [x,X]=run()

clear all;

%load calibration
load R1
load T1
load K

k=K;
t=T1;
r=R1;

dummy();

% 
% %%%%%%%%%%%%%for points
% 
% %Synthesise input 2d point
% x,y = synthesisData('points');
% 
% %plane projection is homography
% h = computeProjection('planar', k, r, t);
% 
% %get 3dpoint
% X = compute3dPts('planar', x, h);
% 
% %clear h to avoiding using it again by mistake
% clear h;
% 
% %Pure translation measurement
% %movement for 1cm on either sides for 3 translations
% ps=-10;
% pe=10;
% for d =ps:pe
%     tc=t;
%     %change paramter
%     tc(1) = t(1)+d;
% 
%     %plane projection is homography
%     h_est = computeProjection('planar', k, r, tc);
% 
%     %get 3dpoint
%     X_est = compute3dPts('planar', x, h_est);
%     
%     %compute difference in new 3d estimate
%     diff(:,d+ps*-1+1)=norm(X-X_est);
% end
% plot(diff)
% 



%%%for cracks
%Synthesise input 2d point
[x,y] = synthesisData('cracks');

%plane projection is homography
h = computeProjection('planar', k, r, t);

%get 3dpoint
X = compute3dPts('planar', x, h);
Y = compute3dPts('planar', y, h);
W=abs(X-Y);
W=W(1,:);

%clear h to avoiding using it again by mistake
clear h;

%Pure translation measurement
%movement for 1cm on either sides for 3 translations
ps=-100;
pe=100;
for d =ps:pe
    tc=t;
    %change paramter
    tc(3) = t(3)+d;

    %plane projection is homography
    h_est = computeProjection('planar', k, r, tc);

    %get 3dpoint
    X_est = compute3dPts('planar', x, h_est);
    Y_est = compute3dPts('planar', y, h_est);
    W_est=abs(X_est-Y_est);
    W_est=W_est(1,:);
    
    %compute difference in new 3d estimate
    diff(:,d+ps*-1+1)=norm(W-W_est);
end
plot(diff)

% %Pure rotation measurement
% %movement for 1degree/radian on either sides for 3 translations
% ps=-10;
% pe=10;
% for d =ps:pe
%     rc=r;
%     ang = rot2ang(rc);
%     
%     %change paramter
%     ang(1) = ang(1)+d*pi/360;
%     rc = ang2rot(ang);
% 
%     %plane projection is homography
%     h_est = computeProjection('planar', k, rc, t);
% 
%     %get 3dpoint
%     X_est = compute3dPts('planar', x, h_est);
%     Y_est = compute3dPts('planar', y, h_est);
%     W_est=abs(X_est-Y_est);
%     W_est=W_est(1,:);
%     
%     %compute difference in new 3d estimate
%     diff(:,d+ps*-1+1)=norm(W-W_est);
% end
% plot(diff)
% 
% 
% 
% 
% 
% 
% function ang = rot2ang(R)
%   
% return
% 
% 
% 
% function rot = ang2rot(ang)
% 
% return
