function h = computeProjection(type, k, r, t)
h=[];
switch (type)
    case 'planar'
        rt=[r(:,1:2) t];
        h=k*rt;
end

return