% % function [x,y] = synthesisData(type)
% % sysnthesis the 2d coordinates based on the type of 2d structure required
% % output = 2d coordinates
% % input = 2dtype
% % % % 2dtype = 'points', 'crackWidth', 'crackheight'
%%%%%%% poitns = '2d', '3d'
function [x,y,zx,zy] = synthesisData(type, points)
x=[];
y=[];
zx=[];
zy=[];

switch(points)
    case '2d'
        %camera resolution 2048 * 1080
        xavg=2048;
        xstd = 2048/2;
        yavg=1080;
        ystd=1080/2;
    case '3d'
        %3dpoints measured from left top corner of checkboard that is
        %0.5mts above the line laser locaiton.
        xavg=500;
        xstd = 0;
        yavg=2048/2;
        ystd=2048;  
        
        count=1;
        while count <= 100
            u=xavg;
            v=randn*(ystd)+yavg;
            if v > ystd || v<0
                'v is out of range';
                continue
            end
            c=[u v]';
            dy=randn*1.5+3;
            dx=randn*0.5+0;
            d = [u+dx v+dy]';
            ny=randn*0.5+0;
            nx=randn*2.5+5;
            e = [u+nx v+ny]';
            ny=randn*0.5+0;
            nx=randn*2.5+5;
            f = [u+dx+nx v+dy+ny]';
            
            if count == 1
                x=c;
                y=d;                
                zx=e;
                zy=f;
            else
                x(:,count)=c;
                y(:,count)=d;
                zx(:,count)=e;
                zy(:,count)=f;
            end

            count=count+1;
        end
        
end


% 
% 
% switch(type)
%     case 'points'
%     
%         count=1;
%         while count <= 100
%             u=randn*(xstd)+xavg;
%             if u > xlim || u<0
%                 'u is out of range'; 
%                 continue
%             end
%             v=randn*(ylim/2)+ylim;
%             if v > ylim || v<0
%                 'v is out of range';
%                 continue
%             end
%             c=[u v]';
%             if count == 1
%                 x=c;
%             else
%                 x(:,count)=c;
%             end
%             count=count+1;
%         end
%         
%     case 'cracks'
%          count=1;
%         while count <= 100
%             u=randn*(xlim/2)+xlim;
%             if u > xlim || u<0
%                 'u is out of range'; 
%                 continue
%             end
%             v=randn*(ylim/2)+ylim;
%             if v > ylim || v<0
%                 'v is out of range';
%                 continue
%             end
%             c=[u v]';
%             dx=randn*3/2+3;
%             dy=randn*0.5+0;
%             d = [u+dx v+dy]';
%             if count == 1
%                 x=c;
%                 y=d;                
%             else
%                 x(:,count)=c;
%                 y(:,count)=d;
%             end
%             count=count+1;
%         end
%      
%         case 'depths'
%          count=1;
%         while count <= 100
%             u=randn*(xlim/2)+xlim;
%             if u > xlim || u<0
%                 'u is out of range'; 
%                 continue
%             end
%             v=randn*(ylim/2)+ylim;
%             if v > ylim || v<0
%                 'v is out of range';
%                 continue
%             end
%             c=[u v]';
%             dx=randn*1.5+3;
%             dy=randn*0.5+0;
%             d = [u+dx v+dy]';
%             nx=randn*0.5+0;
%             ny=randn*2.5+5;
%             e = [u+nx v+ny]';
%             nx=randn*0.5+0;
%             ny=randn*2.5+5;
%             f = [u+dx+nx v+dy+ny]';
%             
%             if count == 1
%                 x=c;
%                 y=d;                
%                 zx=e;
%                 zy=f;
%             else
%                 x(:,count)=c;
%                 y(:,count)=d;
%                 zx(:,count)=e;
%                 zy(:,count)=f;
%             end
% 
%             count=count+1;
%         end
%         
end


