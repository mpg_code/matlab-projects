function X = compute3dPts(type,x,P)
X=[];
switch(type)
    case 'planar' % planar projection
        [r c]=size(P);
        if r~=3 || c~=3
            'Error in compute3dpts: Homogrpahy size is not 3x3'
            return;
        else
            h=P;
        end
        for j=1:length(x)
            homox = [x(:,j); 1];
            homoX=h\homox; % equivalent inv(h)* homox;
            C=[homoX(1)/homoX(3), homoX(2)/homoX(3)]';
            if j==1
                X=C;
            else
                X(:,j)=C;
            end
        end
end

return