% % function []=emulateCam()
% % output: error in the measurement
% % input: X - 3Dcalibration params: type, K, R, T
% % % % 2dtype - 'points', 'cracks', 'depths'

function [X_new]=emulateCam(X, k, r, t, rot, trans)

% X(3)=0;
% X(4)=1;
% rrot = vision.internal.calibration.rodriguesVectorToMatrix(vision.internal.calibration.rodriguesMatrixToVector(r) + rot);
% xcam=[rrot -1.*(rrot*t)]*X;
% 
% X_new=[r -1.*(r*t)]\xcam;
% X_new=X_new./X_new(4);
% X_new(4)=[];


%%% initial check
% if length(find(size(t)==3))!=1 %if rrot is a vector 
    
rrot = vision.internal.calibration.rodriguesVectorToMatrix(vision.internal.calibration.rodriguesMatrixToVector(r) + rot);

% % project to 2d image points based on calib parameters
H = computeHomo(k, rrot, t);
x = H * X; %X is 3x1 with z=0 and H is 3x3

% % back project 2d image points to 3d measurements
Hb = computeHomo(k, r, t);
X_new = Hb\x;
% % [g,h] = transformPointsInverse(tform, x_new(1),x_new(2));
% % X_new=[g h 1];
% % xwrong = Hb*X;
% % % % 'diff and rot'
% % x-xwrong
% % rot.*180./pi
return
    


% % % % % % % % % % % % % % % % % % % % % 
% local functions
% % % % % % % % % % % % % % % % % % % % % 

% % function H = computeHomo(k, r, t)
% % output: intrinsic, rotation and translation matrix
% % input: homogrpahy
function H = computeHomo(k, r, t)
        rt=[r(:,1:2) t];
        H=k*rt;
return

% % function [X,Y,W,H] = get3dMeasurements(h,x,y,zx,zy)
% % output: X,Y = 3d points, W = 3d width, H = 3d height
% % input: h=homography, x,y,zx,zy = 2d points
function [X,Y,W,H] = get3dMeasurements(h,x,y,zx,zy)
    X=[];Y=[];W=[];H=[];
    %get 3d measurements
    X = compute3dPts('planar', x, h);
    Y = compute3dPts('planar', y, h);
    W=abs(X-Y);
    W=W(1,:);
    if (~isempty(zx) && ~isempty(zy))
        ZX = compute3dPts('planar', zx, h);
        ZY = compute3dPts('planar', zy, h);
        H = max(ZX(2)-X(2),ZY(2)-Y(2));
    end
return


