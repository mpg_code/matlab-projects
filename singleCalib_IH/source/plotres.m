function o = plotres(varargin)

varargin{:}
length(varargin);
close all;

load tdiff;
load rdiff;
load taxes;
load raxes;
tmin=taxes(1);
tmax=taxes(end);
rmin=raxes(1);
rmax=raxes(end);

% % % plotting points error for 3 translations
% mt=[tdiff(1,:,1);tdiff(1,:,2);tdiff(1,:,3)];
% phandle = createplot([tmin:tmax], mt);
% xlabel('Camera Translation (in millimeters)');
% ylabel('Point Measurement (in millimeters)');

% % plotting width error for 3 translations
mt=[tdiff(2,:,1);tdiff(2,:,2);tdiff(2,:,3)];
phandle = createplot([tmin:tmax], mt);
xlabel('Camera position mis-alignment (in mm)','FontSize',12,'FontWeight','bold');
ylabel('Error in width measurement (in mm)','FontSize',12,'FontWeight','bold');
set(phandle(1),'Marker','*', 'LineWidth', 1, 'MarkerSize', 3);
set(phandle(2),'Marker','o', 'LineWidth', 1, 'MarkerSize', 3);
set(phandle(3),'Marker','d', 'LineWidth', 1, 'MarkerSize', 3);
axis([-100 100 0 1]);

% % plotting depth error for 3 translations
mt=[tdiff(3,:,1);tdiff(3,:,2);tdiff(3,:,3)];
phandle = createplot([tmin:tmax], mt);
xlabel('Camera position mis-alignment (in mm)','FontSize',12,'FontWeight','bold');
ylabel('Error in depth measurement (in mm)','FontSize',12,'FontWeight','bold');
set(phandle(1),'Marker','*', 'LineWidth', 1, 'MarkerSize', 3);
set(phandle(2),'Marker','o', 'LineWidth', 1, 'MarkerSize', 3);
set(phandle(3),'Marker','d', 'LineWidth', 1, 'MarkerSize', 3);
axis([-100 100 0 1]);

% % % plotting points error for 3 rotations
% mr=[rdiff(1,:,1);rdiff(1,:,2);rdiff(1,:,3)];
% phandle = createplot([rmin:rmax], mr);
% xlabel('Camera Rotation Angle (in degrees)');
% ylabel('Point Measurement (in millimeters)');

% % plotting depth error for 3 rotations
mr=[rdiff(2,:,1);rdiff(2,:,2);rdiff(2,:,3)];
phandle = createplot([rmin:rmax], mr);
xlabel('Camera orientaton mis-alignment (in degrees)','FontSize',12,'FontWeight','bold');
ylabel('Error in width measurement (in mm)','FontSize',12,'FontWeight','bold');
set(phandle(1),'Marker','*', 'LineWidth', 1);
set(phandle(2),'Marker','o', 'LineWidth', 1);
set(phandle(3),'Marker','d', 'LineWidth', 1);
axis([rmin rmax 0 12]);

% % plotting depth error for 3 rotations
mr=[rdiff(3,:,1);rdiff(3,:,2);rdiff(3,:,3)];
phandle = createplot([rmin:rmax], mr);
xlabel('Camera orientaton mis-alignment (in degrees)','FontSize',12,'FontWeight','bold');
ylabel('Error in depth measurement (in mm)','FontSize',12,'FontWeight','bold');
set(phandle(1),'Marker','*', 'LineWidth', 1);
set(phandle(2),'Marker','o', 'LineWidth', 1);
set(phandle(3),'Marker','d', 'LineWidth', 1);
axis([rmin rmax 0 12]);

end

function plot1 = createplot(v,m)
figure1 = figure;
axes1 = axes('Parent',figure1,'YGrid','on');
% ylim(axes1,[0 250]);
box(axes1,'on');
hold(axes1,'all');
plot1 = plot(v,m,'Parent',axes1);
set(plot1(1),'Color',[0 0 1],'DisplayName','x axis');
set(plot1(2),'Color',[0 1 0],'DisplayName','y axis');
set(plot1(3),'Color',[1 0 0],'DisplayName','z axis');
legend5 = legend(axes1,'show');
set(legend5,...
    'Position',[0.472899728997294 0.748436103663986 0.0945121951219512 0.162645218945487]);

end