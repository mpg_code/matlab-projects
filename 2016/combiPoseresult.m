function [r mres]=combiPoseresult()

% path='D:\Users\ddw\Phd\Database\FeatCorr\Current\';
% Reprojection75#102540
% path='D:\Users\ddw\Phd\Database\FeatCorr\pt5-sparsity(1075#2575#25500)\';
path='D:\Users\ddw\Phd\Database\FeatCorr\pt8-sparsity(1075#2575#25500)\';

% path='D:\Users\ddw\Phd\Database\FeatCorr\pt8-3baselines-noisevariationVSnumpts\'
% path='D:\Users\ddw\Phd\Database\FeatCorr\pt5-3baselines-noisevariationVSnumpts\'

files=dir(path);
files(1:2)=[];

%% get the result structure 
% for i=2:2
for i=1:length(files)
    file = dir(strcat(path,files(i).name));
    filepath = strcat(path, files(i).name,'\',file(3).name); 
    r{i}= Extract3Dmars(filepath);
end

%% mean over several samples
[mres sres] =validsum(r);

end

function [newr news] = validsum(r)

    models=length(r);
    [a b d s]=size(r{1}.r);
    path=[];
    for i=1:models
        path= strcat(path,' ',r{i}.path);
    end
    newr.path=path;
    newr.fcnt=r{1}.fcnt;
    newr.noise=r{1}.noise;
    newr.spar=r{1}.spar;
    for v=1:s
        for c=1:d
          for j=1:a
            for k=1:b
                fcnt = [];
                noise = [];
                rot = [];
                tran = [];
                ncc = [];
                mse = [];
%                 rpe = [];
                rx=[];
                ry=[];
                rz=[];
                tx=[];
                ty=[];
                tz=[];
                ortho=[];
                rot3d=[];
                for i=1:models
        %             if(~isnan(r{i}.fcnt(j,k,c)))
        %                 fcnt = [fcnt; r{i}.fcnt(j,k,c)];
        %             end
        %             if(~isnan(r{i}.noise(j,k,c)))
        %                 noise = [noise; r{i}.noise(j,k,c)];
        %             end
                    if(~isnan(r{i}.r(j,k,c,v)))
                        rot= [rot; r{i}.r(j,k,c,v)];
                    end
                    if(~isnan(r{i}.t(j,k,c,v)))
                        tran= [tran; r{i}.t(j,k,c,v)];
                    end
                    if(~isnan(r{i}.ncc(j,k,c,v)))
                        ncc= [ncc; r{i}.ncc(j,k,c,v)];
                    end
                    if(~isnan(r{i}.mse(j,k,c,v)))
                        mse= [mse; r{i}.mse(j,k,c,v)];
                    end
%                     if(~isnan(r{i}.rpe(j,k,c,v)))
%                         rpe= [rpe; r{i}.rpe(j,k,c,v)];
%                     end
%         %rx,ry,rz,tx,ty,tz
                    if(~isnan(r{i}.rx(j,k,c,v)))
                        rx= [rx; r{i}.rx(j,k,c,v)];
                    end    
                    if(~isnan(r{i}.ry(j,k,c,v)))
                        ry= [ry; r{i}.ry(j,k,c,v)];
                    end
                    if(~isnan(r{i}.rz(j,k,c,v)))
                        rz= [rz; r{i}.rz(j,k,c,v)];
                    end
                    if(~isnan(r{i}.tx(j,k,c,v)))
                        tx= [tx; r{i}.tx(j,k,c,v)];
                    end    
                    if(~isnan(r{i}.ty(j,k,c,v)))
                        ty= [ty; r{i}.ty(j,k,c,v)];
                    end
                    if(~isnan(r{i}.tz(j,k,c,v)))
                        tz= [tz; r{i}.tz(j,k,c,v)];
                    end
                    if(~isnan(r{i}.ortho(j,k,c,v)))
                        ortho= [ortho; r{i}.ortho(j,k,c,v)];
                    end
                    if(~isnan(r{i}.rot3d(j,k,c,v)))
                        rot3d= [rot3d; r{i}.rot3d(j,k,c,v)];
                    end

                end
                newr.r(j,k,c,v)= mean(rot);
                news.r(j,k,c,v)= std(rot);
                newr.t(j,k,c,v)= mean(tran);
                news.t(j,k,c,v)= std(tran);
                newr.ncc(j,k,c,v)= mean(ncc);
                news.ncc(j,k,c,v)= std(ncc);
                newr.mse(j,k,c,v)= mean(mse);
                news.mse(j,k,c,v)= std(mse);
%                 newr.rpe(j,k,c,v)= mean(rpe);
%                 news.rpe(j,k,c,v)= std(rpe);
                newr.rx(j,k,c,v)= mean(rx);
                news.rx(j,k,c,v)= std(rx);
                newr.ry(j,k,c,v)= mean(ry);        
                news.ry(j,k,c,v)= std(ry);        
                newr.rz(j,k,c,v)= mean(rz);
                news.rz(j,k,c,v)= std(rz);
                newr.tx(j,k,c,v)= mean(tx);
                news.tx(j,k,c,v)= std(tx);
                newr.ty(j,k,c,v)= mean(ty);        
                news.ty(j,k,c,v)= std(ty);        
                newr.tz(j,k,c,v)= mean(tz);
                news.tz(j,k,c,v)= std(tz);
                newr.ortho(j,k,c,v)= mean(ortho);
                news.ortho(j,k,c,v)= std(ortho);
                newr.rot3d(j,k,c,v)= mean(rot3d);
                news.rot3d(j,k,c,v)= std(rot3d);
            end
          end
        end
    end
end