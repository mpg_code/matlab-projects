function o =lineup(mdmatrix, col, tit, xlab, ylab)

% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
%% Uncomment the following line to preserve the X-limits of the axes
xlim(axes1,[0 4]);
box(axes1,'on');
hold(axes1,'on');
% Create ylabel
ylabel(ylab,'FontSize',14);
% Create xlabel
xlabel(xlab,'FontSize',14);
% Create title
title(tit,'FontSize',14);

mat=[mdmatrix(:,col,1) mdmatrix(:,col,2) mdmatrix(:,col,3)];
plot(ones(6,1), mat(:,1),'r*-');
hold on;plot(ones(6,1)*2, mat(:,2),'b*-');
plot(ones(6,1)*3, mat(:,3),'g*-');hold off
end