function plotPose()

[r res]=combiPoseresult();

close all;

%%feature points wihtout noise
lineup(res.mse,1,'3D point position accuracy','Baselines(10,25,40), Noise=0','3D MSE');
lineup(res.rot3d,1,'3D point orientation accuracy','Baselines(10,25,40), Noise=0','3D rotation');
% without sparsity
spar = 1;
%%noise and various feature points at baseline 10
plotPoseSpecific(res.rot3d(:,:,1)','rot3d')
plotPoseSpecific(res.mse(:,:,1)','pos3d')
meshing(res.ortho(:,:,1))

%%noise and various feature points at baseline 25
plotPoseSpecific(res.rot3d(:,:,2)','rot3d')
plotPoseSpecific(res.mse(:,:,2)','pos3d')
meshing(res.ortho(:,:,2))

%%noise and various feature points at baseline 40
plotPoseSpecific(res.rot3d(:,:,3)','rot3d')
plotPoseSpecific(res.mse(:,:,3)','pos3d')
meshing(res.ortho(:,:,3))

%%RT
plotPoseSpecific(res.r(:,:,1)','rerr')
plotPoseSpecific(res.t(:,:,1)','terr')

plotPoseSpecific(res.r(:,:,2)','rerr')
plotPoseSpecific(res.t(:,:,2)','terr')

plotPoseSpecific(res.r(:,:,3)','rerr')
plotPoseSpecific(res.t(:,:,3)','terr')

%% for sparsity
% for s = 1:6
% sparmse(s,:)=res.mse(3,:,2,s);
% end

%% B=25, N=75 and 500, noise=sparsity=ALL
for s = 1:6
    sparmse75_10(s,:)=res.mse(3,:,1,s);
    sparrot75_10(s,:)=res.rot3d(3,:,1,s);
    sparortho75_10(s,:)=res.ortho(3,:,1,s);
    
    sparmse75_25(s,:)=res.mse(3,:,2,s);
    sparrot75_25(s,:)=res.rot3d(3,:,2,s);
    sparortho75_25(s,:)=res.ortho(3,:,2,s);

    sparmse500_25(s,:)=res.mse(5,:,2,s);
    sparrot500_25(s,:)=res.rot3d(5,:,2,s);
    sparortho500_25(s,:)=res.ortho(5,:,2,s);

end
plotPoseSpecific(sparmse75_10','pos3dspar');
plotPoseSpecific(sparmse75_25','pos3dspar');
plotPoseSpecific(sparmse500_25','pos3dspar');

plotPoseSpecific(sparrot75_10','rot3dspar');
plotPoseSpecific(sparrot75_25','rot3dspar');
plotPoseSpecific(sparrot500_25','rot3dspar');

meshing(sparortho75_10);
meshing(sparortho75_25);
meshing(sparortho500_25);


%%%comparing 3d error with rp err for N=75pts, baseline 10,25,40, sparsity
%%%= 100 and noise varying
comp10=[res.mse(3,:,1);res.rpe(3,:,1)];
figure, plot(comp10');
comp25=[res.mse(3,:,2);res.rpe(3,:,2)];
figure, plot(comp25');
comp40=[res.mse(3,:,3);res.rpe(3,:,3)];
figure, plot(comp40');

%%noise and various feature points at baseline 10
plotPoseSpecific(res.mse(:,:,1)','pos3d')
plotPoseSpecific(res.rpe(:,:,1)','rperr')
meshing(res.ortho(:,:,1))

%%noise and various feature points at baseline 25
plotPoseSpecific(res.mse(:,:,2)','pos3d')
plotPoseSpecific(res.rpe(:,:,2)','rperr')
meshing(res.ortho(:,:,2))

%%noise and various feature points at baseline 40
plotPoseSpecific(res.mse(:,:,3)','pos3d')
plotPoseSpecific(res.rpe(:,:,3)','rperr')
meshing(res.ortho(:,:,3))

end
