function [r mres]=combiSiftresult()

path='D:\Users\ddw\Phd\Database\FeatCorr\Current\';
% Reprojection75#102540
% path='D:\Users\ddw\Phd\Database\FeatCorr\pt5-sparsity(1075#2575#25500)\';
% path='D:\Users\ddw\Phd\Database\FeatCorr\pt8-sparsity(1075#2575#25500)\';

% path='D:\Users\ddw\Phd\Database\FeatCorr\pt8-3baselines-noisevariationVSnumpts\'
% path='D:\Users\ddw\Phd\Database\FeatCorr\pt5-3baselines-noisevariationVSnumpts\'

files=dir(path);
files(1:2)=[];

%% get the result structure 
% for i=2:2
for i=1:length(files)
    file = dir(strcat(path,files(i).name));
    filepath = strcat(path, files(i).name,'\',file(3).name); 
    r{i}= Extract3Dmars(filepath);
end

%% mean over several samples
[mres sres] =validsum(r);


%%plotting
close all;
figure1=figure;
axes1 = axes('Parent',figure1);
plot1 = plot([mres.mse(:,10) mres.mse(:,25) mres.mse(:,40)]);
set(axes1,'XTickLabel',{'1','2','3','4','5'});
ylabel('3D Position error','FontSize',14);
xlabel('Descriptor distance (Mean squared)','FontSize',14);
set(plot1(1),'DisplayName','Small','Color',[1 0 0]);
set(plot1(2),'DisplayName','Medium','Color',[0 1 0]);
set(plot1(3),'DisplayName','Large','Color',[0 0 1]);
legend(axes1,'show');

figure2=figure;
axes2 = axes('Parent',figure2);
plot2 = plot([mres.rot3d(:,10) mres.rot3d(:,25) mres.rot3d(:,40)]);
set(axes2,'XTickLabel',{'1','2','3','4','5'});
ylabel('3D Rotation error','FontSize',14);
xlabel('Descriptor distance (Mean squared)','FontSize',14);
set(plot2(1),'DisplayName','Small','Color',[1 0 0]);
set(plot2(2),'DisplayName','Medium','Color',[0 1 0]);
set(plot2(3),'DisplayName','Large','Color',[0 0 1]);
legend(axes2,'show');

figure3=figure;
axes3 = axes('Parent',figure3);
plot3 = plot([mres.fcnt(:,10) mres.fcnt(:,25) mres.fcnt(:,40)]);
set(axes3,'XTickLabel',{'1','2','3','4','5'});
ylabel('Number of Points','FontSize',14);
xlabel('Descriptor distance (Mean squared)','FontSize',14);
set(plot3(1),'DisplayName','Small','Color',[1 0 0]);
set(plot3(2),'DisplayName','Medium','Color',[0 1 0]);
set(plot3(3),'DisplayName','Large','Color',[0 0 1]);
legend(axes3,'show');

meshing([mres.ortho(:,10) mres.ortho(:,25) mres.ortho(:,40)]);

end

function [newr news] = validsum(r)

models=length(r);
[a b]=size(r{1}.fcnt);
path=[];
for i=1:models
    path= strcat(path,' ',r{i}.path);
end
newr.path=path;
news.path=path;
for j=1:a
    for k=1:b
        fcnt = [];
        perr = [];
        rot = [];
        tran = [];
        ncc = [];
        mse = [];
        rx=[];
        ry=[];
        rz=[];
        tx=[];
        ty=[];
        tz=[];
        ortho=[];
        rot3d=[];
        for i=1:models
            if(~isnan(r{i}.fcnt(j,k)))
                fcnt = [fcnt; r{i}.fcnt(j,k)];
            end
            if(~isnan(r{i}.perr(j,k)))
                perr= [perr; r{i}.perr(j,k)];
            end
            if(~isnan(r{i}.r(j,k)))
                rot= [rot; r{i}.r(j,k)];
            end
            if(~isnan(r{i}.t(j,k)))
                tran= [tran; r{i}.t(j,k)];
            end
            if(~isnan(r{i}.ncc(j,k)))
                ncc= [ncc; r{i}.ncc(j,k)];
            end
            if(~isnan(r{i}.mse(j,k)))
                mse= [mse; r{i}.mse(j,k)];
            end
%rx,ry,rz,tx,ty,tz
            if(~isnan(r{i}.rx(j,k)))
                rx= [rx; r{i}.rx(j,k)];
            end    
            if(~isnan(r{i}.ry(j,k)))
                ry= [ry; r{i}.ry(j,k)];
            end
            if(~isnan(r{i}.rz(j,k)))
                rz= [rz; r{i}.rz(j,k)];
            end
            if(~isnan(r{i}.tx(j,k)))
                tx= [tx; r{i}.tx(j,k)];
            end    
            if(~isnan(r{i}.ty(j,k)))
                ty= [ty; r{i}.ty(j,k)];
            end
            if(~isnan(r{i}.tz(j,k)))
                tz= [tz; r{i}.tz(j,k)];
            end
            if(~isnan(r{i}.ortho(j,k)))
                ortho= [ortho; r{i}.ortho(j,k)];
            end
            if(~isnan(r{i}.rot3d(j,k)))
                rot3d= [rot3d; r{i}.rot3d(j,k)];
            end
        end
        newr.fcnt(j,k)= mean(fcnt);
        news.fcnt(j,k)= std(fcnt);
        newr.perr(j,k)= mean(perr);
        news.perr(j,k)= std(perr);
        newr.r(j,k)= mean(rot);
        news.r(j,k)= std(rot);
        newr.t(j,k)= mean(tran);
        news.t(j,k)= std(tran);
        newr.ncc(j,k)= mean(ncc);
        news.ncc(j,k)= std(ncc);
        newr.mse(j,k)= mean(mse);
        news.mse(j,k)= std(mse);
        newr.rx(j,k)= mean(rx);
        news.rx(j,k)= std(rx);
        newr.ry(j,k)= mean(ry);        
        news.ry(j,k)= std(ry);        
        newr.rz(j,k)= mean(rz);
        news.rz(j,k)= std(rz);
        newr.tx(j,k)= mean(tx);
        news.tx(j,k)= std(tx);
        newr.ty(j,k)= mean(ty);        
        news.ty(j,k)= std(ty);        
        newr.tz(j,k)= mean(tz);
        news.tz(j,k)= std(tz);
        newr.ortho(j,k)= mean(ortho);
        news.ortho(j,k)= std(ortho);
        newr.rot3d(j,k)= mean(rot3d);
        news.rot3d(j,k)= std(rot3d);
    end
end

end