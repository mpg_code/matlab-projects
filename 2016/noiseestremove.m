function out = noiseestremove()
load('imgpts.mat');
im1 = imgpts(:,1:2);
im2 = imgpts(:,3:4);
diffsq = (im2-im1).^2;
sdist = sqrt(sum(diffsq')');

%add noise
noise = 4;
nim2(:,1) = im2(:,1) + randn(10,1)*noise;
nim2(:,2) = im2(:,2) + randn(10,1)*noise;
% nim2 = im2 + randn(10,2)*noise;
ndiffsq = (nim2-im1).^2;
ndist = sqrt(sum(ndiffsq')');

rdist = estrem(ndist);
% %est noise
% [m s] = est(ndist)
% 
% %remove noise
% for i=1:100
%     r1(:,i) = nim2(:,1) - (m+randn(10,1)*s);
%     r2(:,i) = nim2(:,2) - (m+randn(10,1)*s);
% end
% rim2 = [mean(r1')' mean(r2')']
% %     rim2 = im2 - randn(10,2)*s;
% rdiffsq = (rim2-im1).^2;
% rdist = sqrt(sum(rdiffsq')');

out=[sdist ndist rdist];
end
% 
% 
% function [m s] = est(in)
% 
% [sns I] = sort(in);
% c=polyfit([1:10]',sns,2);
% y=c(1)*[1:10]'.^2+c(2)*[1:10]'+c(3);
% 
% diff = sns-y;
% m= mean(diff)
% s=std(diff)
% 
% end


function out = estrem(in)

[sns I] = sort(in);
figure, plot(sns,'r');

c=polyfit([1:10]',sns,2);
y=c(1)*[1:10]'.^2+c(2)*[1:10]'+c(3);
hold on;plot(y,'g')

diff = sns-y;
m= mean(diff)
s=std(diff)

for i= 1: 100
    rdist(:,i) = sns - (m+randn(10,1)*s);
end
out = mean(rdist')';
plot(out,'b')
end
