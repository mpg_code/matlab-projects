close all;
bases={'10', '25', '40'};
% bases={'10'};

for b = 1:length(bases)
    base = bases{b};
    n1=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\normsiftest_1_',base,'.txt'));
    n1=reshape(n1,128,length(n1)/128);
    n2=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\normsiftest_2_',base,'.txt'));
    n2=reshape(n2,128,length(n2)/128);
    un1=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\unnormsiftest_1_',base,'.txt'));
    un1=reshape(un1,128,length(un1)/128);
    un2=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\unnormsiftest_2_',base,'.txt'));
    un2=reshape(un2,128,length(un2)/128);

    i1=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\index_1_',base,'.txt'));
    i2=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\index_2_',base,'.txt'));
    d=dlmread(strcat('D:\Users\ddw\Phd\opencv_projects\3DMARS\3DMARS\testfiles\distance_',base,'.txt'));

    en1=n1(:,i1+1);
    en2=n2(:,i2+1);
    eun1=un1(:,i1+1);
    eun2=un2(:,i2+1);

    dn=[];
    dun=[];
%% squared root of sum of squared difference 
    for f=1:length(d)
        dn(f)=sqrt(sum((en1(:,f)-en2(:,f)).^2));
        dun(f)=sqrt(sum((eun1(:,f)-eun2(:,f)).^2));
    end
%  
% %% sum of difference
%     for f=1:length(d)
%         dn(f)=sum(en1(:,f)-en2(:,f));
%         dun(f)=sum(eun1(:,f)-eun2(:,f));
%     end
%     
% %% sum of absolute difference
%     for f=1:length(d)
%         dn(f)=sum(abs(en1(:,f)-en2(:,f)));
%         dun(f)=sum(abs(eun1(:,f)-eun2(:,f)));
%     end
    
    dn=dn/128;
    dun=dun/128;
    
%     %post process
%     a=mean(dun);
%     b=std(dun);
%     dun=dun(find(dun>a-b & dun < a+b));
    
    

    [mean(dn) std(dn) d]
    figure,plot(dn)
    
    minpos = find(dn==min(dn))
    figure,plot(en1(:,minpos));
    hold on; plot(en2(:,minpos),'g'); hold off;

    maxpos = find(dn==max(dn))
    figure,plot(en1(:,maxpos));
    hold on; plot(en2(:,maxpos),'r'); hold off;

end