%%testfunc



%----------------------------------------------------
%Create Matrices
%----------------------------------------------------

% modelview matrix - world coord is same as model coord
% rc=Rodrigues(R);
% cc=T;
% mv=[rc' -1*rc'*cc; 
%     0 0 0 1];
mv=[Rodrigues(R)' -1*Rodrigues(R)'*T; 0 0 0 1];

%projection matrix
f = tand(fov/2); %focal for unit height
right = f * w/h * near;
left = -right;
top = f * near;
down = -top;
A= (right + left) /(right -left);
B = (top+down)/(top-down);
C = -(far+near)/(far-near);
D = -2 * far * near /(far-near);

pm = [2*near/(right-left) 0 A 0; 
      0 2*near/(top-down) B 0; 
      0 0 C D;
      0 0 -1 0];
%projection matrix with reverse z direction
rpm=pm;
rpm(1,1)=-rpm(1,1);
rpm(2,2)=-rpm(2,2);
rpm(3,3)=-rpm(3,3);
rpm(3,4)=-rpm(3,4);
rpm(4,3)=-rpm(4,3);
% %projection matrix with reverse z direction
% % converting from RHS to LHS
% near = -near;
% far = -far;
% right = f * w/h * near;
% left = -right;
% top = f * near;
% down = -top;
% A= (right + left) /(right -left);
% B = (top+down)/(top-down);
% C = -(far+near)/(far-near);
% D = -2 * far * near /(far-near);
% 
% rpm = [2*near/(right-left) 0 A 0; 
%       0 2*near/(top-down) B 0; 
%       0 0 C D;
%       0 0 -1 0];
  
%modelviewprojection matrix
mvp = pm * mv;

% Matrix to convert from NDC to Screen
sm = [w/2 0 0 w/2;
      0 -h/2 0 h/2;
      0 0 1 0;
      0 0 0 1];
  
% % Matrix to convert from NDC to Screen
% rsm = [2/w 0 0 -1;
%      0 -2/h 0 1;
%      0 0 1 0;
%      0 0 0 1]
%  
 %inverse of matrices
imv = inv(mv); % inverse modelview matrix
ipm = inv(pm); % inverse projection matrix
imvp = inv(mvp); % inverse modelviewprojection matrix
ism = inv(sm); % inverse of NDC to Screen


%%conversion from RHS to LHS
m=[1 0 0 0; 0 1 0 0; 0 0 -1 0; 0 0 0 1];
nworld = m*world;
nR = -R;
nT = T;
nT(3) = -nT(3);

% m=[-1 0 0 0; 0 -1 0 0; 0 0 -1 0; 0 0 0 1];
% nworld = m*world;
% nR = R;
% nT = -T;

nmv=[Rodrigues(nR)' -1*Rodrigues(nR)'*nT; 0 0 0 1];
k=sm*pm;
k(2,2)=k(1,1);
k(1,3)=w/2;
k(2,3)=h/2;
k(3,3)=1;
k(3,4)=0;
k(4,3)=0;
k(4,4)=1;

inmv = inv(nmv);
ik = inv(k);
%----------------------------------------------------
%Projection
%----------------------------------------------------

%from world to camera
cam = mv * world;
ncam = nmv * nworld;

% from cam to clip
clip = pm * cam; 
% %or straight from world to clip => 
% clip = mvp * world;
% homofactor = mvp(end,:)*world; % or clip(4)

% from clip to ndc
homofactor = pm(end,:)*cam; % or clip(4)
ndc=clip/homofactor;
% %or straight from world to ndc => 
% homofactor = mvp(end,:)*world;
%  (mvp * world)/homofactor

% from ndc to screen
% (ndc.x + 1)* w/2;
% (1-ndc.y) * h/2;
scr = sm*ndc;

%_________________________________________
%%compare with groundtruth screen coord
Screenerror = norm(screen(1:2)-scr(1:2))
%_________________________________________


%----------------------------------------------------
%Back Projection
%----------------------------------------------------

%from screen to ndc
rndc = ism*scr; % here we preserve the z component

%from ndc to clip
rhomofactor = 1/sum(ipm(end,:)*rndc);
rclip = rndc*(rhomofactor);

%from clip to cam
rcam = ipm * rclip;

%from cam to world
rworld = imv* rcam;

% %or straight from clip to world => 
% rworld = imvp * rclip
% %or straight from ndc to world => 
% rhomofactor = 1/sum(imvp(end,:)*rndc);
% rworld = imvp * (rndc*(rhomofactor));

%_________________________________________
%%compare with groundtruth world coord
Worlderror = norm(world(1:3)-rworld(1:3))
%_________________________________________


%----------------------------------------------------
% Composing camera instrinsics from projection matrix
%----------------------------------------------------
k = sm*pm;
ik = inv(k);
p2d = k * cam;
p2d = p2d/p2d(4);
Screenerror = norm(p2d(1:2)-scr(1:2))
nrcam = ik * p2d;
nrcam = nrcam/nrcam(4);
InstinsicCOmposeError = norm(nrcam - cam)

%----------------------------------------------------
% Composing Total Projection matrix
%----------------------------------------------------
k = sm*pm;
kmv = k*mv;
ikmv = inv(kmv);
p2d = kmv * world;
p2d = p2d/p2d(4)
Screenerror = norm(p2d(1:2)-scr(1:2))
rworld = ikmv * p2d;
rworld = rworld/rworld(4)
Worlderror = norm(world(1:3)-rworld(1:3))

%----------------------------------------------------
% A new K = [aspect*fy 0 cx; 0 fy cy; 0 0 1 ]
%----------------------------------------------------
aspect = w/h;
fy = h/(2*tand(fov/2));
cx=w/2;
cy=h/2;
newk = [-1*aspect*fy 0 cx; 0 fy cy; 0 0 1];
newk=[newk [0 0 0]';0 0 0 1];
knewmv = newk*mv;
iknewmv = inv(knewmv);

p2new = knewmv * world;
p2new = p2new/p2new(3)
Screenerror = norm(p2new(1:2)-scr(1:2))
rnewworld = iknewmv * p2new;
rnewworld = rnewworld/rnewworld(4)
Worlderror = norm(world(1:3)-rnewworld(1:3))
