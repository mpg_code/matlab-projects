function [m1 m2 m3 m4] = stat(x)

m1 = min(x);
m2 = max(x);
m3 = mean(x);
m4 = std(x);

end