function out = msestat(vec)
    m=[];
    s=[];
    for i = 1: size(vec,1)
        nvec=vec(i,find(~isnan(vec(i,:))));
        m=[m; mean(nvec)];
        s=[s; std(nvec)];
    end
    out =[m s];
end