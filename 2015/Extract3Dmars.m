% function [Afnt perr dr dt ncc time mse] = Extract3Dmars(path)
function [out] = Extract3Dmars(path)

h1path = strsplit(path,'_');
h2path = strsplit(h1path{2},'.');
resultype = str2num(h2path{1});
sparsity=1;
switch(resultype)
    case 0
        
        [combi pair Bfnt...
    Afnt time...
    gr1 gr2 gr3 gt1 gt2 gt3...
    r1 r2 r3 t1 t2 t3...
    perr ncc mse sam...
    o1x o1y o1z o2x o2y o2z o3x o3y o3z o4x o4y o4z...
    e1x e1y e1z e2x e2y e2z e3x e3y e3z e4x e4y e4z]=...
    textread(path,'%f %f %f %f %f [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] %f %f %f %f [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f] [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f]');

    case 1            
    [pair fcount noise spar...
    gr1 gr2 gr3 gt1 gt2 gt3...
    r1 r2 r3 t1 t2 t3...
    ncc mse ...
    o1x o1y o1z o2x o2y o2z o3x o3y o3z o4x o4y o4z...
    e1x e1y e1z e2x e2y e2z e3x e3y e3z e4x e4y e4z]=...
    textread(path,'%f %f %f %f [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] %f %f [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f] [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f]');

    case 2            
    [pair thresh fcount...
    gr1 gr2 gr3 gt1 gt2 gt3...
    r1 r2 r3 t1 t2 t3...
    perr ncc mse ...
    o1x o1y o1z o2x o2y o2z o3x o3y o3z o4x o4y o4z...
    e1x e1y e1z e2x e2y e2z e3x e3y e3z e4x e4y e4z]=...
    textread(path,'%f %f %f [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] %f %f %f [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f] [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f]');

end

% [combi pair Bfnt...
%     Afnt time...
%     gr1 gr2 gr3 gt1 gt2 gt3...
%     r1 r2 r3 t1 t2 t3...
%     perr ncc mse ...
%     o1x o1y o1z o2x o2y o2z o3x o3y o3z o4x o4y o4z...
%     e1x e1y e1z e2x e2y e2z e3x e3y e3z e4x e4y e4z]=...
%     textread(path,'%f %f %f %f %f [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] [%f,%f,%f] %f %f %f [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f] [%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f]');

%%rotation and translation data
Rg = [gr1 gr2 gr3]';
Tg = [gt1 gt2 gt3]';
Re = [r1 r2 r3]';
Te = [t1 t2 t3]';
drint=abs(Rg-Re);
rdiff.x=drint(1,:);
rdiff.y=drint(2,:);
rdiff.z=drint(3,:);

dtint=abs(Tg-Te);
tdiff.x=dtint(1,:);
tdiff.y=dtint(2,:);
tdiff.z=dtint(3,:);

dr=sum(drint);
dt=sum(dtint);

%%3D point data
for j=1:length(o1x)
ori3d= [ 
       o1x(j) o1y(j) o1z(j);
       o2x(j) o2y(j) o2z(j);
       o3x(j) o3y(j) o3z(j);
       o4x(j) o4y(j) o4z(j)];
   est3d = [
       e1x(j) e1y(j) e1z(j);
       e2x(j) e2y(j) e2z(j);
       e3x(j) e3y(j) e3z(j);
       e4x(j) e4y(j) e4z(j)];   
    
   if(sum(sum(ori3d)==0) || sum(sum(est3d)==0))
        orthotol(j) = NaN;
        rot3d(j) = NaN;
    continue
   end;
       
   %%work with 3d estimation data
    oveca=ori3d(1,:)-ori3d(2,:);
    ovecb=ori3d(1,:)-ori3d(3,:);
    ovecc=ori3d(1,:)-ori3d(4,:);
    ova=sqrt(sum((oveca).^2));
    ovb=sqrt(sum((ovecb).^2));
    ovc=sqrt(sum((ovecc).^2));
    oang = sum(abs([90,90,90]-[real(acos(sum(oveca.*ovecb)/ova*ovb)), real(acos(sum(ovecb.*ovecc)/ovb*ovc)), real(acos(sum(ovecc.*oveca)/ovc*ova))].*(180/pi)));

    eveca=est3d(1,:)-est3d(2,:);
    evecb=est3d(1,:)-est3d(3,:);
    evecc=est3d(1,:)-est3d(4,:);
    eva=sqrt(sum((eveca).^2));
    evb=sqrt(sum((evecb).^2));
    evc=sqrt(sum((evecc).^2));
%     eang = sum(abs([90,90,90]-[real(acos(sum(eveca.*evecb)/eva*evb)), real(acos(sum(evecb.*evecc)/evb*evc)), real(acos(sum(evecc.*eveca)/evc*eva))].*(180/pi)));
    vec = [real(acos(sum(eveca.*evecb)/eva*evb)), real(acos(sum(evecb.*evecc)/evb*evc)), real(acos(sum(evecc.*eveca)/evc*eva))].*(180/pi);
%     eang = sum([90-vec(find(vec<90)) mod(vec(find(vec>90)),90)]);
    if(resultype==0)
        minv=70;maxv=110;
    end
    if(resultype==1 || resultype==2)
        minv=88;maxv=92;            
    end
    
    if(length(find(vec>minv&vec<maxv))==3)
        eang=1;
    else
        eang=0;
    end
    orthotol(j) = eang;  
    %rotation
    centerdiff = est3d(1,:)-ori3d(1,:);
    nest3d = est3d-repmat(centerdiff,4,1);
    nveca=nest3d(1,:)-nest3d(2,:);
    nvecb=nest3d(1,:)-nest3d(3,:);
    nvecc=nest3d(1,:)-nest3d(4,:);
    nva=sqrt(sum((nveca).^2));
    nvb=sqrt(sum((nvecb).^2));
    nvc=sqrt(sum((nvecc).^2));
%     nang = mean([real(acos(sum(nveca.*oveca)/nva*ova)), real(acos(sum(nvecb.*ovecb)/nvb*ovb)), real(acos(sum(nvecc.*ovecc)/nvc*ovc))].*(180/pi));
    nang = mean([abs(real(asin(sum(cross(nveca,oveca))/nva*ova))), abs(real(asin(sum(cross(nvecb,ovecb))/nvb*ovb))), abs(real(asin(sum(cross(nvecc,ovecc))/nvc*ovc)))].*(180/pi));

    
    rot3d(j) = nang;    

end

%% into matrix format
if(resultype==0)
    for i=1:length(combi)    
        
        drmat(combi(i),pair(i))=dr(i);
        rxmat(combi(i),pair(i))=rdiff.x(i);
        rymat(combi(i),pair(i))=rdiff.y(i);
        rzmat(combi(i),pair(i))=rdiff.z(i);

        dtmat(combi(i),pair(i))=dt(i);
        txmat(combi(i),pair(i))=tdiff.x(i);
        tymat(combi(i),pair(i))=tdiff.y(i);
        tzmat(combi(i),pair(i))=tdiff.z(i);

        if(ncc(i)==1e10)ncc(i)=NaN;end
        nccmat(combi(i),pair(i))=ncc(i);
        if(mse(i)==1e10)mse(i)=NaN;end
        msemat(combi(i),pair(i))=mse(i);
        if(sam(i)==1e10)sam(i)=NaN;end
        sammat(combi(i),pair(i))=sam(i);
        orthomat(combi(i),pair(i))=orthotol(i);
        rot3dmat(combi(i),pair(i))=rot3d(i);

       
        if(perr(i)==1e10)perr(i)=NaN;end
        perrmat(combi(i),pair(i))=perr(i);
        if(time(i)==1e10)time(i)=NaN;end
        timemat(combi(i),pair(i))=time(i);
        Bfntmat(combi(i),pair(i))=Bfnt(i);
        Afntmat(combi(i),pair(i))=Afnt(i);
    end
    out.path = path;
    out.r = drmat;
    out.t = dtmat;
    out.ncc= nccmat;
    out.mse =msemat;
    out.sam = sammat;
    
    out.rx = rxmat;
    out.ry = rymat;
    out.rz = rzmat;
    out.tx = txmat;
    out.ty = tymat;
    out.tz = tzmat;

    out.ortho = orthomat;
    out.rot3d = rot3dmat;
    out.fcnt=Afntmat;
    out.perr = perrmat;    
    out.time = timemat;
end

if(resultype==1)
   fcnt=[10,50,75,100,500,1000];
   noi=[0:2:10];
   st=[10 25 40];
   spread=[1 2 3 4 5 6];
   for j=1:length(fcount)
       r = find(fcnt==fcount(j));
       c = find(noi==noise(j));
       p = find(st==pair(j));
       s = find(spread==spar(j));
       
        drmat(r,c,p,s)=dr(j);
        rxmat(r,c,p,s)=rdiff.x(j);
        rymat(r,c,p,s)=rdiff.y(j);
        rzmat(r,c,p,s)=rdiff.z(j);

        dtmat(r,c,p,s)=dt(j);
        txmat(r,c,p,s)=tdiff.x(j);
        tymat(r,c,p,s)=tdiff.y(j);
        tzmat(r,c,p,s)=tdiff.z(j);

        if(ncc(j)==1e10)ncc(j)=NaN;end
        nccmat(r,c,p,s)=ncc(j);
        if(mse(j)==1e10)mse(j)=NaN;end
        msemat(r,c,p,s)=mse(j);
%         if(rpe(j)==1e10)rpe(j)=NaN;end
%         rpemat(r,c,p,s)=rpe(j);
        orthomat(r,c,p,s)=orthotol(j);
        rot3dmat(r,c,p,s)=rot3d(j);
 end
   
    out.path = path;
    out.fcnt = fcnt;
    out.noise = noi;
    out.spar = spread;
    out.r = drmat;
    out.t = dtmat;
    out.ncc= nccmat;
    out.mse =msemat;
%     out.rpe = rpemat;
    
    out.rx = rxmat;
    out.ry = rymat;
    out.rz = rzmat;
    out.tx = txmat;
    out.ty = tymat;
    out.tz = tzmat;

    out.ortho = orthomat;
    out.rot3d = rot3dmat;
end

if(resultype==2)
%     limits=[0.5:0.5:5];
    limits=[1:5];
    for i=1:length(thresh)    
        th=find(limits==thresh(i));
        drmat(th,pair(i))=dr(i);
        rxmat(th,pair(i))=rdiff.x(i);
        rymat(th,pair(i))=rdiff.y(i);
        rzmat(th,pair(i))=rdiff.z(i);

        dtmat(th,pair(i))=dt(i);
        txmat(th,pair(i))=tdiff.x(i);
        tymat(th,pair(i))=tdiff.y(i);
        tzmat(th,pair(i))=tdiff.z(i);

        if(ncc(i)==1e10)ncc(i)=NaN;end
        nccmat(th,pair(i))=ncc(i);
        if(mse(i)==1e10)mse(i)=NaN;end
        msemat(th,pair(i))=mse(i);
        orthomat(th,pair(i))=orthotol(i);
        rot3dmat(th,pair(i))=rot3d(i);

       
        if(perr(i)==1e10)perr(i)=NaN;end
        perrmat(th,pair(i))=perr(i);
        fcnt(th,pair(i))=fcount(i);
    end
    out.path = path;
    out.r = drmat;
    out.t = dtmat;
    out.ncc= nccmat;
    out.mse =msemat;
    out.perr = perrmat;
    
    out.rx = rxmat;
    out.ry = rymat;
    out.rz = rzmat;
    out.tx = txmat;
    out.ty = tymat;
    out.tz = tzmat;

    out.ortho = orthomat;
    out.rot3d = rot3dmat;
    out.fcnt=fcnt;
end

end
