function [rout mout mgrp sgrp Penalty_FE Penalty_FunMat Penalty_PE]=combiresult()

%  path='D:\Users\ddw\Phd\Database\FeatCorr\Current\';


% path='D:\Users\ddw\Phd\Database\FeatCorr\base1deg_unitvec3destimation\';

% path='D:\Users\ddw\Phd\Database\FeatCorr\base1deg_unitvec\';
% path='D:\Users\ddw\Phd\Database\FeatCorr\base1deg_modelpoints100\';


% path='D:\Users\ddw\Phd\Database\FeatCorr\testold\';
% path='D:\Users\ddw\Phd\Database\FeatCorr\Current\';
% path='D:\Users\ddw\Phd\Database\FeatCorr\test\';

path='D:\Users\ddw\Phd\Database\FeatCorr\MMsysSampson\';

files=dir(path);
files(1:2)=[];

grouping = 5;
removeoutlier = 0;
validatePE=1;

%% get the result structure 
% for i=2:2
for i=1:length(files)
    file = dir(strcat(path,files(i).name));
    filepath = strcat(path, files(i).name,'\',file(3).name); 
    r{i}= Extract3Dmars(filepath);
end

%% penalty for feature extraction  + funmat est.
penperr=[];
penncc=[];
for i=1:length(r)
    penperr(i,:) = sum(isnan(r{i}.perr)');
    penncc(i,:) = sum(isnan(r{i}.ncc)');
end
Penalty_FE = sum(penncc)';
Penalty_FunMat = (sum(penperr)-Penalty_FE')';

%% validate pose estimation error (this is not doen in c++ code so required here. 
% Pose estimation returns one of the four possible solutions, but due to
% incorrect feature mathing, selected solution is geometrically garbage.
if(validatePE==1)
    rout = validate_PE(r);
    pen=[];
    for i=1:length(r)
        pen(i,:) = sum(isnan(rout{i}.ncc)');
    end
    Penalty_PE = sum(pen)'; 
else
    rout=r;
    Penalty_PE=0;
end

%% mean over several samples
[mres sres] =validsum(rout);

%% outlier removal
if(removeoutlier==1)
    [mout sout] =remoutlier(mres,sres);
else
    mout=mres;
    sout=sres;
end
%% groups them for different baselines 
mgrp = groupup(mout,grouping);
sgrp = groupup(sout,grouping);
% mgrp=[];
% sgrp=[];


end

function [out] = validate_PE(r)
out=r;
listpermodel=[];
    for i = 1: length(r)
%         a=[];
%         b=[];
%         c=[];
%         list=[];
%         a=find(r{i}.rx>90);
%         b=find(r{i}.ry>90);
%         c=find(r{i}.rz>90);
%         list=a;
%         for j=1:length(b)
%             if(isempty(find(list==b(j))))
%                 list=[list;b(j)];
%             end
%         end
%         for k=1:length(c)
%             if(isempty(find(list==c(k))))
%                 list=[list;c(k)];
%             end
%         end
        list=find(r{i}.rx>90|r{i}.ry>90|r{i}.rz>90|r{i}.tx>1|r{i}.ty>1|r{i}.tz>1)
        %only few parameters are affected by this penalty for the results.
        % ex. we want to show R adn T as it is with penalities but filter
        % the data to show Ncc and MSe adn rot3d.
        % and time 
        % for table data.
        
%         out{i}.fcnt(list)=NaN;
%         out{i}.perr(list)=NaN;
%         out{i}.r(list)=NaN;
%         out{i}.t(list)=NaN;
        out{i}.ncc(list)=NaN;
        out{i}.time(list)=NaN;
        out{i}.mse(list)=NaN;
        out{i}.sam(list)=NaN;
%         out{i}.rx(list)=NaN;
%         out{i}.ry(list)=NaN;
%         out{i}.rz(list)=NaN;
%         out{i}.tx(list)=NaN;
%         out{i}.ty(list)=NaN;
%         out{i}.tz(list)=NaN;
        out{i}.ortho(list)=NaN;
        out{i}.rot3d(list)=NaN;
        listpermodel{i}=list;
    end
end
function [newr snewr] = groupup(r, factor)
if(factor==1)
    newr=[];
    snewr=[];
    return;
end

[newr.path]= r.path;
[snewr.path]= r.path;
[newr.fcnt snewr.fcnt]= grouping(r.fcnt, factor);
[newr.perr snewr.perr]= grouping(r.perr, factor);
[newr.r snewr.r]= grouping(r.r, factor);
[newr.t snewr.t]= grouping(r.t, factor);
[newr.ncc snewr.ncc]= grouping(r.ncc, factor);
[newr.time snewr.time]= grouping(r.time, factor);
[newr.mse snewr.mse]= grouping(r.mse, factor);
[newr.sam snewr.sam]= grouping(r.sam, factor);

[newr.rx snewr.rx]= grouping(r.rx, factor);
[newr.ry snewr.ry]= grouping(r.ry, factor);
[newr.rz snewr.rz]= grouping(r.rz, factor);
[newr.tx snewr.tx]= grouping(r.tx, factor);
[newr.ty snewr.ty]= grouping(r.ty, factor);
[newr.tz snewr.tz]= grouping(r.tz, factor);

[newr.ortho snewr.ortho]= grouping(r.ortho, factor);
[newr.rot3d snewr.rot3d]= grouping(r.rot3d, factor);

end
function [mout sout] =remoutlier(mres,sres)

%vecmse = mres.mse;
nanvec = mres.mse;
%first level
nanvec(find(nanvec>1e4))=NaN;
%second level
for j = 1:size(nanvec,1)
   vec = nanvec(j,:);
   nonvec = find(~isnan(vec));
   thresh = mean(nonvec)+1*std(nonvec);
%    thresh = 100;
   vec(find(vec> thresh))=NaN;
   nanvec(j,:) = vec;
end
nanpos = find(isnan(nanvec));

mres.fcnt(nanpos) = NaN;
mres.perr(nanpos) = NaN;
mres.rot(nanpos) = NaN;
mres.tran(nanpos) = NaN;
mres.ncc(nanpos) = NaN;
mres.time(nanpos) = NaN;
mres.mse(nanpos) = NaN;
mres.rx(nanpos) = NaN;
mres.ry(nanpos) = NaN;
mres.rz(nanpos) = NaN;
mres.tx(nanpos) = NaN;
mres.ty(nanpos) = NaN;
mres.tz(nanpos) = NaN;

sres.fcnt(nanpos) = NaN;
sres.perr(nanpos) = NaN;
res.rot(nanpos) = NaN;
sres.tran(nanpos) = NaN;
sres.ncc(nanpos) = NaN;
sres.time(nanpos) = NaN;
sres.mse(nanpos) = NaN;
sres.rx(nanpos) = NaN;
sres.ry(nanpos) = NaN;
sres.rz(nanpos) = NaN;
sres.tx(nanpos) = NaN;
sres.ty(nanpos) = NaN;
sres.tz(nanpos) = NaN;

mout = mres;
sout=sres;

end
function [newr news] = validsum(r)

models=length(r);
[a b]=size(r{1}.fcnt);
path=[];
for i=1:models
    path= strcat(path,' ',r{i}.path);
end
newr.path=path;
news.path=path;
for j=1:a
    for k=1:b
        fcnt = [];
        perr = [];
        rot = [];
        tran = [];
        ncc = [];
        time = [];
        mse = [];
        sam = [];
        rx=[];
        ry=[];
        rz=[];
        tx=[];
        ty=[];
        tz=[];
        ortho=[];
        rot3d=[];
        for i=1:models
            if(~isnan(r{i}.fcnt(j,k)))
                fcnt = [fcnt; r{i}.fcnt(j,k)];
            end
            if(~isnan(r{i}.perr(j,k)))
                perr= [perr; r{i}.perr(j,k)];
            end
            if(~isnan(r{i}.r(j,k)))
                rot= [rot; r{i}.r(j,k)];
            end
            if(~isnan(r{i}.t(j,k)))
                tran= [tran; r{i}.t(j,k)];
            end
            if(~isnan(r{i}.ncc(j,k)))
                ncc= [ncc; r{i}.ncc(j,k)];
            end
            if(~isnan(r{i}.time(j,k)))
                time= [time; r{i}.time(j,k)];
            end
            if(~isnan(r{i}.mse(j,k)))
                mse= [mse; r{i}.mse(j,k)];
            end
            if(~isnan(r{i}.sam(j,k)))
                sam= [sam; r{i}.sam(j,k)];
            end
%rx,ry,rz,tx,ty,tz
            if(~isnan(r{i}.rx(j,k)))
                rx= [rx; r{i}.rx(j,k)];
            end    
            if(~isnan(r{i}.ry(j,k)))
                ry= [ry; r{i}.ry(j,k)];
            end
            if(~isnan(r{i}.rz(j,k)))
                rz= [rz; r{i}.rz(j,k)];
            end
            if(~isnan(r{i}.tx(j,k)))
                tx= [tx; r{i}.tx(j,k)];
            end    
            if(~isnan(r{i}.ty(j,k)))
                ty= [ty; r{i}.ty(j,k)];
            end
            if(~isnan(r{i}.tz(j,k)))
                tz= [tz; r{i}.tz(j,k)];
            end
            if(~isnan(r{i}.ortho(j,k)))
                ortho= [ortho; r{i}.ortho(j,k)];
            end
            if(~isnan(r{i}.rot3d(j,k)))
                rot3d= [rot3d; r{i}.rot3d(j,k)];
            end
        end
        newr.fcnt(j,k)= mean(fcnt);
        news.fcnt(j,k)= std(fcnt);
        newr.perr(j,k)= mean(perr);
        news.perr(j,k)= std(perr);
        newr.r(j,k)= mean(rot);
        news.r(j,k)= std(rot);
        newr.t(j,k)= mean(tran);
        news.t(j,k)= std(tran);
        newr.ncc(j,k)= mean(ncc);
        news.ncc(j,k)= std(ncc);
        newr.time(j,k)= mean(time);
        news.time(j,k)= std(time);
        newr.mse(j,k)= mean(mse);
        news.mse(j,k)= std(mse);
        newr.sam(j,k)= mean(sam);
        news.sam(j,k)= std(sam);
        newr.rx(j,k)= mean(rx);
        news.rx(j,k)= std(rx);
        newr.ry(j,k)= mean(ry);        
        news.ry(j,k)= std(ry);        
        newr.rz(j,k)= mean(rz);
        news.rz(j,k)= std(rz);
        newr.tx(j,k)= mean(tx);
        news.tx(j,k)= std(tx);
        newr.ty(j,k)= mean(ty);        
        news.ty(j,k)= std(ty);        
        newr.tz(j,k)= mean(tz);
        news.tz(j,k)= std(tz);
        newr.ortho(j,k)= mean(ortho);
        news.ortho(j,k)= std(ortho);
        newr.rot3d(j,k)= mean(rot3d);
        news.rot3d(j,k)= std(rot3d);
    end
end

end
function [m s] = grouping(mat, factor)

m=[];
s=[];
grp=factor;
step = floor(length(mat)/grp);
for i=1:step
    vec = mat(:,(i-1)*grp+1 : i*grp);
    mint=[];
    sint=[];
    for index=1:size(vec,1)
       nvec=vec(index,find(~isnan(vec(index,:))));
       mint = [mint; mean(nvec)];
       sint = [sint; std(nvec)];
    end
    m=[m mint];
    s=[s sint];
end
end


% function newr = sumup(r)
% 
% models=length(r);
% [a b]=size(r{1}.fcnt);
% path=[];
% fcnt = zeros(a,b);
% perr = zeros(a,b);
% rot = zeros(a,b);
% tran = zeros(a,b);
% ncc = zeros(a,b);
% time = zeros(a,b);
% mse = zeros(a,b);
% 
% for i=1:models
%     path= strcat(path,' ',r{i}.path);
%     fcnt= (fcnt + r{i}.fcnt);
%     perr= (perr + r{i}.perr);
%     rot= (rot + r{i}.r);
%     tran= (tran + r{i}.t);
%     ncc= (ncc + r{i}.ncc);
%     time= (time + r{i}.time);
%     mse= (mse + r{i}.mse);
% end
% 
% newr.path=path;
% newr.fcnt= (fcnt)./length(r);
% newr.perr= (perr)./length(r);
% newr.r= (rot)./length(r);
% newr.t= (tran)./length(r);
% newr.ncc= (ncc)./length(r);
% newr.time= (time)./length(r);
% newr.mse= (mse)./length(r);
% 
% end

% 
% plotSpecific(res.fcnt,'fpts')
% plotSpecific(res.perr,'perr')
% plotSpecific(res.time,'time')
% 
% plotSpecific(res.r,'rerr',[1:6])
% plotSpecific(res.r,'rerr',[7 9 11])
% plotSpecific(res.r,'rerr',[8 10 12])
% plotSpecific(res.t,'terr',[1:6])
% plotSpecific(res.t,'terr',[7 9 11])
% plotSpecific(res.t,'terr',[8 10 12])
% 
% plotSpecific(res.ncc,'nerr',[1:6])
% plotSpecific(res.ncc,'nerr',[7 9 11])
% plotSpecific(res.ncc,'nerr',[8 10 12])
% 
