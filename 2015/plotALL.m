[r resori mgrp sgrp pfe pfmat ppe]=combiresult();

close all;
res=mgrp;

own = [1:7 26:27];
brief = [1 8 10 12];
freak = [1 9 11 13];
sifts = [2 14 15];
surfs = [3 16 17];
brisks = [4 18 19];
orbs = [5 20 21];
kazes = [6 22 23];
akazes = [7 24 25];

allbriefs = [1 8 10 12 14 16 18 20 22 24];
allfreaks = [1 9 11 13 15 17 19 21 23 25];
all = [1:27];
xrange = [2.5:5:50];



% saveas(g1,'realtest.eps','eps2c')

% plotSpecific(res.perr,'perr')
% plotSpecific(res.time,'time')

%% scatter plots
n=res.ncc(2:end,:);
[a b]=size(n);
c=ones(a,b);
for i=1:b
c(:,i)=c(:,i)*i;
end

f=res.fcnt(2:end,:);
figure,scatter(f(:),n(:),[],c(:),'MarkerFaceColor','Flat')
% scatter(f(:),n(:));
xlim([0 1000]);
ylim([0 1]);
xlabel('Total matched features','FontSize',20);
ylabel('Normalized correlation coefficient','FontSize',20);
title('3D accuracy versus matched features','FontSize',20);
   
s=mgrp.perr(2:end,:)
figure,scatter(s(:),n(:),[],c(:),'MarkerFaceColor','Flat')
% scatter(s(:),n(:));
xlim([-0.5 25]);
ylim([0 1]);
xlabel('Pixel error (in pixels)','FontSize',20);
ylabel('Normalized correlation coefficient','FontSize',20);
title('3D accuracy versus 2D pixel error','FontSize',20);


s=mgrp.sam(2:end,:)
figure,scatter(s(:),n(:),[],c(:),'MarkerFaceColor','Flat')
% scatter(s(:),n(:));
xlim([-0.5 25]);
ylim([0 1]);
xlabel('Squared Sampson error (in pixels)','FontSize',20);
ylabel('Normalized correlation coefficient','FontSize',20);
title('3D accuracy versus 2D error','FontSize',20);

pause


%% bar plots
% % plotSpecific(sgrp.ncc(2:end,:)','std', :, xrange)
%plotSpecific(sgrp.ncc,'std',:, xrange)

% 
% figure, bar(xrange,sgrp.ncc(2:end,:)')
% ylim([0 1]);
% xlabel('Baseline (angular separation in degrees)','FontSize',12);
% ylabel('NCC - Standard deviation','FontSize',12);
% title('Variation of standard deviation of NCC over baseline','FontSize',12);

% sd = sgrp.ncc(own,:);
% sd(1,:)=[];
% figure, bar(xrange,sd')
% ylim([0 1]);
% xlabel('Baseline (angular separation in degrees)','FontSize',12);
% ylabel('NCC - Standard deviation','FontSize',12);
% title('Variation of standard deviation of NCC over baseline','FontSize',12);
% 
% sd = sgrp.ncc(allbriefs,:);
% sd(1,:)=[];
% figure, bar(xrange,sd')
% ylim([0 1]);
% xlabel('Baseline (angular separation in degrees)','FontSize',12);
% ylabel('NCC - Standard deviation','FontSize',12);
% title('Variation of standard deviation of NCC over baseline','FontSize',12);
% 
% sd = sgrp.ncc(allfreaks,:);
% sd(1,:)=[];
% figure, bar(xrange,sd')
% ylim([0 1]);
% xlabel('Baseline (angular separation in degrees)','FontSize',12);
% ylabel('NCC - Standard deviation','FontSize',12);
% title('Variation of standard deviation of NCC over baseline','FontSize',12);

pause
%%linear plots

plotSpecific(res.fcnt,'fpts')
% plotSpecific(res.perr,'perr')

plotSpecific(res.perr,'perr',all,xrange)

pause

plotSpecific(res.r,'rerr',own,xrange)
plotSpecific(res.r,'rerr',brief,xrange)
plotSpecific(res.r,'rerr',freak,xrange)

plotSpecific(res.t,'terr',own,xrange)
plotSpecific(res.t,'terr',brief,xrange)
plotSpecific(res.t,'terr',freak,xrange)

plotSpecific(res.ncc,'nerr',own,xrange)
plotSpecific(res.ncc,'nerr',brief,xrange)
plotSpecific(res.ncc,'nerr',freak,xrange)

% plotSpecific(res.mse,'merr',own)
% plotSpecific(res.mse,'merr',brief)
% plotSpecific(res.mse,'merr',freak)


plotSpecific(res.ncc,'nerr',sifts,xrange)
plotSpecific(res.ncc,'nerr',surfs,xrange)
plotSpecific(res.ncc,'nerr',brisks,xrange)
plotSpecific(res.ncc,'nerr',orbs,xrange)
plotSpecific(res.ncc,'nerr',kazes,xrange)
plotSpecific(res.ncc,'nerr',akazes,xrange)



%%mse calculation - 3 baselines low medium high with time as fourth column
dres = [msestat(res.mse(:,2:4)) msestat(res.mse(:,5:7)) msestat(res.mse(:,8:10))];
rres = [msestat(res.rot3d(:,2:4)) msestat(res.rot3d(:,5:7)) msestat(res.rot3d(:,8:10))];
ores = [msestat(res.ortho(:,:))];

% dres = [msestat(resori.mse(:,6:20)) msestat(resori.mse(:,21:35)) msestat(resori.mse(:,36:50))]
combi={'IDEAL','SIFT','SURF','BRISK','ORB','KAZE','AKAZE',...
       'MSER-BRIEF','MSER-FREAK',...
       'STAR-BRIEF','STAR-FREAK',...
       'FAST-BRIEF','FAST-FREAK',...
       'SIFT-BRIEF','SIFT-FREAK',...
       'SURF-BRIEF','SURF-FREAK',...
       'BRISK-BRIEF','BRISK-FREAK',...
       'ORB-BRIEF','ORB-FREAK',...
       'KAZE-BRIEF','KAZE-FREAK',...
       'AKAZE-BRIEF','AKAZE-FREAK',...
       'MSER-SURF','STAR-SURF',...
       };
fstr=[];

%penalties
% TotalPenalty = Penalty_PE'+Penalty_FE'+Penalty_FunMat';
%%penalties
TotalPenalty =  pfe + pfmat + ppe;
TotalPenalty(find(TotalPenalty>450))=450;
conf = (1-TotalPenalty/450)*100;
featpen = (pfe'+pfmat')/450;
posepen = ppe'/450;


t = msestat(res.time);
fd=fopen('table.txt','w');
fd2=fopen('table2.txt','w');
for j = 1: size(dres,1)
    d = dres(j,:);
    rr = rres(j,:);
    or = ores(j,:);
%         str = strcat(combi{j},' & ',num2str(d(1)),' & ',num2str(d(2)),' & ',num2str(d(3)),...
%         ' & ',num2str(d(4)),' & ',num2str(d(5)),' & ',num2str(d(6)),' & ', num2str(t(j,1)),'\\');

%         str = strcat(combi{j},...
%             ' & ',num2str(rr(1)),'(',(num2str(rr(2))),')',' & ',num2str(d(1)),'(',(num2str(d(2))),')',...
%             ' & ',num2str(rr(3)),'(',(num2str(rr(4))),')',' & ',num2str(d(3)),'(',(num2str(d(4))),')',...
%             ' & ',num2str(rr(6)),'(',(num2str(rr(6))),')',' & ',num2str(d(5)),'(',(num2str(d(6))),')',...
%             ' & ', num2str(t(j,1)),' & ',num2str(Conf(j)),' & ',num2str(or(1)),'\\');

%         str=sprintf('%s & %.3g (%.3g) & %.3g (%.3g) & %.3g (%.3g) & %.3g (%.3g) & %.3g (%.3g) & %.3g (%.3g) & %.3g & %.3g & %.3g\\\\',...
        str=sprintf('%s & %.2f (%.2f) & %.2f (%.2f) & %.2f (%.2f) & %.2f (%.2f) & %.2f (%.2f) & %.2f (%.2f)& %.2f & %.2f\\\\',...
            combi{j},rr(1),rr(2),d(1),d(2),rr(3),rr(4),d(3),d(4),rr(5),rr(6),d(5),d(6),t(j,1),conf(j));
        fprintf(fd,'%s\n',str);
        
        str2 = sprintf('%s & %.2f & %.3f & %.3f & %.2f & %.2f\\\\',combi{j},t(j,1),featpen(j),posepen(j),conf(j),or(1));
        fprintf(fd2,'%s\n',str2);
        
%     fstr{j}=str;
end
fclose(fd);
fclose(fd2);
